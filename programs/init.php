<?php


use Cofy\Utilities\FunctionalityTools\BabFunctionalities;
use function Cofy\Utilities\Addon\bab_getAddonInfosInstance;

function jquery_upgrade($version_base, $version_ini)
{
	$functionalities = new BabFunctionalities();
	
	$addon = bab_getAddonInfosInstance('jquery');

	if (false === $functionalities->register('Jquery', $addon->getPhpPath() . 'jquery.php')) {
		return false; 
	}

	return true;
}



function jquery_onDeleteAddon() {

	$functionalities = new BabFunctionalities();

	if (false === $functionalities->unregister('Jquery')) {
		return false;
	}

	return true;
}

// function jquery_onPageRefreshed()
// {
// 	global $babBody;
// 	$addon = bab_getAddonInfosInstance('jquery');
// 	$baseScriptsPath = $addon->getPhpPath();
// 	$stylesheetFile = $baseScriptsPath.('css/jquery.lightbox.css');
// 	$babBody->addstylesheetFile($stylesheetFile);	
	
// 	// 	require_once dirname(__FILE__) . '/functions.php';
// 	$addon = bab_getAddonInfosInstance('jquery');
// 	$baseScriptsPath = $addon->getPhpPath();
// 	$javascriptFile = $baseScriptsPath.'js/jquery.lightbox.js';
// 	$babBody->addJavascriptFile($javascriptFile);

// }