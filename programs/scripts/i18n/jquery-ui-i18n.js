function bab_jQuery_extractUrlParams(){
    var getParams = {};
    jQuery('script').each(function(){
        var src = jQuery(this).attr("src");
        if(src){
            if( src.indexOf("jquery-ui-i18n.js") > -1){
                var getarray = src.split("?")[1].split("&");
                for (key = 0; key < getarray.length; ++key) {
                    var currentVal = getarray[key];
                    var val = currentVal.split('=');
                    getParams[val[0]] = val[1];
                }
            }
        }
    });

    return getParams;
}
//TRY USING https://r12a.github.io/apps/conversion/ to convert chars
//UPDATE VERSION BELOW THIS LINE / ! \ THERE IS CODE TO KEEP at the end of this file when updated

/*! jQuery UI - v1.11.4 - 2015-03-11
* http://jqueryui.com
* Includes: datepicker-af.js, datepicker-ar-DZ.js, datepicker-ar.js, datepicker-az.js, datepicker-be.js, datepicker-bg.js, datepicker-bs.js, datepicker-ca.js, datepicker-cs.js, datepicker-cy-GB.js, datepicker-da.js, datepicker-de.js, datepicker-el.js, datepicker-en-AU.js, datepicker-en-GB.js, datepicker-en-NZ.js, datepicker-eo.js, datepicker-es.js, datepicker-et.js, datepicker-eu.js, datepicker-fa.js, datepicker-fi.js, datepicker-fo.js, datepicker-fr-CA.js, datepicker-fr-CH.js, datepicker-fr.js, datepicker-gl.js, datepicker-he.js, datepicker-hi.js, datepicker-hr.js, datepicker-hu.js, datepicker-hy.js, datepicker-id.js, datepicker-is.js, datepicker-it-CH.js, datepicker-it.js, datepicker-ja.js, datepicker-ka.js, datepicker-kk.js, datepicker-km.js, datepicker-ko.js, datepicker-ky.js, datepicker-lb.js, datepicker-lt.js, datepicker-lv.js, datepicker-mk.js, datepicker-ml.js, datepicker-ms.js, datepicker-nb.js, datepicker-nl-BE.js, datepicker-nl.js, datepicker-nn.js, datepicker-no.js, datepicker-pl.js, datepicker-pt-BR.js, datepicker-pt.js, datepicker-rm.js, datepicker-ro.js, datepicker-ru.js, datepicker-sk.js, datepicker-sl.js, datepicker-sq.js, datepicker-sr-SR.js, datepicker-sr.js, datepicker-sv.js, datepicker-ta.js, datepicker-th.js, datepicker-tj.js, datepicker-tr.js, datepicker-uk.js, datepicker-vi.js, datepicker-zh-CN.js, datepicker-zh-HK.js, datepicker-zh-TW.js
* Copyright 2015 jQuery Foundation and other contributors; Licensed MIT */

(function( factory ) {
    if ( typeof define === "function" && define.amd ) {

        // AMD. Register as an anonymous module.
        define([ "jquery" ], factory );
    } else {

        // Browser globals
        factory( jQuery );
    }
}(function( $ ) {

var datepicker = $.datepicker;

/* Afrikaans initialisation for the jQuery UI date picker plugin. */
/* Written by Renier Pretorius. */


datepicker.regional['af'] = {
    closeText: 'Selekteer',
    prevText: 'Vorige',
    nextText: 'Volgende',
    currentText: 'Vandag',
    monthNames: ['Januarie','Februarie','Maart','April','Mei','Junie',
    'Julie','Augustus','September','Oktober','November','Desember'],
    monthNamesShort: ['Jan', 'Feb', 'Mrt', 'Apr', 'Mei', 'Jun',
    'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Des'],
    dayNames: ['Sondag', 'Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrydag', 'Saterdag'],
    dayNamesShort: ['Son', 'Maa', 'Din', 'Woe', 'Don', 'Vry', 'Sat'],
    dayNamesMin: ['So','Ma','Di','Wo','Do','Vr','Sa'],
    weekHeader: 'Wk',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['af']);

var i18nDatepickerAf = datepicker.regional['af'];


/* Algerian Arabic Translation for jQuery UI date picker plugin. (can be used for Tunisia)*/
/* Mohamed Cherif BOUCHELAGHEM -- cherifbouchelaghem@yahoo.fr */



datepicker.regional['ar-DZ'] = {
    closeText: '\u0625\u063A\u0644\u0627\u0642',
    prevText: '&#x3C;\u0627\u0644\u0633\u0627\u0628\u0642',
    nextText: '\u0627\u0644\u062A\u0627\u0644\u064A&#x3E;',
    currentText: '\u0627\u0644\u064A\u0648\u0645',
    monthNames: ['\u062C\u0627\u0646\u0641\u064A', '\u0641\u064A\u0641\u0631\u064A', '\u0645\u0627\u0631\u0633', '\u0623\u0641\u0631\u064A\u0644', '\u0645\u0627\u064A', '\u062C\u0648\u0627\u0646',
    '\u062C\u0648\u064A\u0644\u064A\u0629', '\u0623\u0648\u062A', '\u0633\u0628\u062A\u0645\u0628\u0631','\u0623\u0643\u062A\u0648\u0628\u0631', '\u0646\u0648\u0641\u0645\u0628\u0631', '\u062F\u064A\u0633\u0645\u0628\u0631'],
    monthNamesShort: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
    dayNames: ['\u0627\u0644\u0623\u062D\u062F', '\u0627\u0644\u0627\u062B\u0646\u064A\u0646', '\u0627\u0644\u062B\u0644\u0627\u062B\u0627\u0621', '\u0627\u0644\u0623\u0631\u0628\u0639\u0627\u0621', '\u0627\u0644\u062E\u0645\u064A\u0633', '\u0627\u0644\u062C\u0645\u0639\u0629', '\u0627\u0644\u0633\u0628\u062A'],
    dayNamesShort: ['\u0627\u0644\u0623\u062D\u062F', '\u0627\u0644\u0627\u062B\u0646\u064A\u0646', '\u0627\u0644\u062B\u0644\u0627\u062B\u0627\u0621', '\u0627\u0644\u0623\u0631\u0628\u0639\u0627\u0621', '\u0627\u0644\u062E\u0645\u064A\u0633', '\u0627\u0644\u062C\u0645\u0639\u0629', '\u0627\u0644\u0633\u0628\u062A'],
    dayNamesMin: ['\u0627\u0644\u0623\u062D\u062F', '\u0627\u0644\u0627\u062B\u0646\u064A\u0646', '\u0627\u0644\u062B\u0644\u0627\u062B\u0627\u0621', '\u0627\u0644\u0623\u0631\u0628\u0639\u0627\u0621', '\u0627\u0644\u062E\u0645\u064A\u0633', '\u0627\u0644\u062C\u0645\u0639\u0629', '\u0627\u0644\u0633\u0628\u062A'],
    weekHeader: '\u0623\u0633\u0628\u0648\u0639',
    dateFormat: 'dd/mm/yy',
    firstDay: 6,
        isRTL: true,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['ar-DZ']);

var i18nDatepickerArDz = datepicker.regional['ar-DZ'];


/* Arabic Translation for jQuery UI date picker plugin. */
/* Used in most of Arab countries, primarily in Bahrain, Kuwait, Oman, Qatar, Saudi Arabia and the United Arab Emirates, Egypt, Sudan and Yemen. */
/* Written by Mohammed Alshehri -- m@dralshehri.com */



datepicker.regional['ar'] = {
    closeText: '\u0625\u063A\u0644\u0627\u0642',
    prevText: '&#x3C;\u0627\u0644\u0633\u0627\u0628\u0642',
    nextText: '\u0627\u0644\u062A\u0627\u0644\u064A&#x3E;',
    currentText: '\u0627\u0644\u064A\u0648\u0645',
    monthNames: ['\u064A\u0646\u0627\u064A\u0631', '\u0641\u0628\u0631\u0627\u064A\u0631', '\u0645\u0627\u0631\u0633', '\u0623\u0628\u0631\u064A\u0644', '\u0645\u0627\u064A\u0648', '\u064A\u0648\u0646\u064A\u0648',
    '\u064A\u0648\u0644\u064A\u0648', '\u0623\u063A\u0633\u0637\u0633', '\u0633\u0628\u062A\u0645\u0628\u0631', '\u0623\u0643\u062A\u0648\u0628\u0631', '\u0646\u0648\u0641\u0645\u0628\u0631', '\u062F\u064A\u0633\u0645\u0628\u0631'],
    monthNamesShort: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
    dayNames: ['\u0627\u0644\u0623\u062D\u062F', '\u0627\u0644\u0627\u062B\u0646\u064A\u0646', '\u0627\u0644\u062B\u0644\u0627\u062B\u0627\u0621', '\u0627\u0644\u0623\u0631\u0628\u0639\u0627\u0621', '\u0627\u0644\u062E\u0645\u064A\u0633', '\u0627\u0644\u062C\u0645\u0639\u0629', '\u0627\u0644\u0633\u0628\u062A'],
    dayNamesShort: ['\u0623\u062D\u062F', '\u0627\u062B\u0646\u064A\u0646', '\u062B\u0644\u0627\u062B\u0627\u0621', '\u0623\u0631\u0628\u0639\u0627\u0621', '\u062E\u0645\u064A\u0633', '\u062C\u0645\u0639\u0629', '\u0633\u0628\u062A'],
    dayNamesMin: ['\u062D', '\u0646', '\u062B', '\u0631', '\u062E', '\u062C', '\u0633'],
    weekHeader: '\u0623\u0633\u0628\u0648\u0639',
    dateFormat: 'dd/mm/yy',
    firstDay: 0,
        isRTL: true,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['ar']);

var i18nDatepickerAr = datepicker.regional['ar'];


/* Azerbaijani (UTF-8) initialisation for the jQuery UI date picker plugin. */
/* Written by Jamil Najafov (necefov33@gmail.com). */


datepicker.regional['az'] = {
    closeText: 'Ba\u011Fla',
    prevText: '&#x3C;Geri',
    nextText: '\u0130r\u0259li&#x3E;',
    currentText: 'Bug\u00FCn',
    monthNames: ['Yanvar','Fevral','Mart','Aprel','May','\u0130yun',
    '\u0130yul','Avqust','Sentyabr','Oktyabr','Noyabr','Dekabr'],
    monthNamesShort: ['Yan','Fev','Mar','Apr','May','\u0130yun',
    '\u0130yul','Avq','Sen','Okt','Noy','Dek'],
    dayNames: ['Bazar','Bazar ert\u0259si','\u00C7\u0259r\u015F\u0259nb\u0259 ax\u015Fam\u0131','\u00C7\u0259r\u015F\u0259nb\u0259','C\u00FCm\u0259 ax\u015Fam\u0131','C\u00FCm\u0259','\u015E\u0259nb\u0259'],
    dayNamesShort: ['B','Be','\u00C7a','\u00C7','Ca','C','\u015E'],
    dayNamesMin: ['B','B','\u00C7','\u0421','\u00C7','C','\u015E'],
    weekHeader: 'Hf',
    dateFormat: 'dd.mm.yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['az']);

var i18nDatepickerAz = datepicker.regional['az'];


/* Belarusian initialisation for the jQuery UI date picker plugin. */
/* Written by Pavel Selitskas <p.selitskas@gmail.com> */


datepicker.regional['be'] = {
    closeText: '\u0417\u0430\u0447\u044B\u043D\u0456\u0446\u044C',
    prevText: '&larr;\u041F\u0430\u043F\u044F\u0440.',
    nextText: '\u041D\u0430\u0441\u0442.&rarr;',
    currentText: '\u0421\u0451\u043D\u044C\u043D\u044F',
    monthNames: ['\u0421\u0442\u0443\u0434\u0437\u0435\u043D\u044C','\u041B\u044E\u0442\u044B','\u0421\u0430\u043A\u0430\u0432\u0456\u043A','\u041A\u0440\u0430\u0441\u0430\u0432\u0456\u043A','\u0422\u0440\u0430\u0432\u0435\u043D\u044C','\u0427\u044D\u0440\u0432\u0435\u043D\u044C',
    '\u041B\u0456\u043F\u0435\u043D\u044C','\u0416\u043D\u0456\u0432\u0435\u043D\u044C','\u0412\u0435\u0440\u0430\u0441\u0435\u043D\u044C','\u041A\u0430\u0441\u0442\u0440\u044B\u0447\u043D\u0456\u043A','\u041B\u0456\u0441\u0442\u0430\u043F\u0430\u0434','\u0421\u044C\u043D\u0435\u0436\u0430\u043D\u044C'],
    monthNamesShort: ['\u0421\u0442\u0443','\u041B\u044E\u0442','\u0421\u0430\u043A','\u041A\u0440\u0430','\u0422\u0440\u0430','\u0427\u044D\u0440',
    '\u041B\u0456\u043F','\u0416\u043D\u0456','\u0412\u0435\u0440','\u041A\u0430\u0441','\u041B\u0456\u0441','\u0421\u044C\u043D'],
    dayNames: ['\u043D\u044F\u0434\u0437\u0435\u043B\u044F','\u043F\u0430\u043D\u044F\u0434\u0437\u0435\u043B\u0430\u043A','\u0430\u045E\u0442\u043E\u0440\u0430\u043A','\u0441\u0435\u0440\u0430\u0434\u0430','\u0447\u0430\u0446\u044C\u0432\u0435\u0440','\u043F\u044F\u0442\u043D\u0456\u0446\u0430','\u0441\u0443\u0431\u043E\u0442\u0430'],
    dayNamesShort: ['\u043D\u0434\u0437','\u043F\u043D\u0434','\u0430\u045E\u0442','\u0441\u0440\u0434','\u0447\u0446\u0432','\u043F\u0442\u043D','\u0441\u0431\u0442'],
    dayNamesMin: ['\u041D\u0434','\u041F\u043D','\u0410\u045E','\u0421\u0440','\u0427\u0446','\u041F\u0442','\u0421\u0431'],
    weekHeader: '\u0422\u0434',
    dateFormat: 'dd.mm.yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['be']);

var i18nDatepickerBe = datepicker.regional['be'];


/* Bulgarian initialisation for the jQuery UI date picker plugin. */
/* Written by Stoyan Kyosev (http://svest.org). */


datepicker.regional['bg'] = {
    closeText: '\u0437\u0430\u0442\u0432\u043E\u0440\u0438',
    prevText: '&#x3C;\u043D\u0430\u0437\u0430\u0434',
    nextText: '\u043D\u0430\u043F\u0440\u0435\u0434&#x3E;',
    nextBigText: '&#x3E;&#x3E;',
    currentText: '\u0434\u043D\u0435\u0441',
    monthNames: ['\u042F\u043D\u0443\u0430\u0440\u0438','\u0424\u0435\u0432\u0440\u0443\u0430\u0440\u0438','\u041C\u0430\u0440\u0442','\u0410\u043F\u0440\u0438\u043B','\u041C\u0430\u0439','\u042E\u043D\u0438',
    '\u042E\u043B\u0438','\u0410\u0432\u0433\u0443\u0441\u0442','\u0421\u0435\u043F\u0442\u0435\u043C\u0432\u0440\u0438','\u041E\u043A\u0442\u043E\u043C\u0432\u0440\u0438','\u041D\u043E\u0435\u043C\u0432\u0440\u0438','\u0414\u0435\u043A\u0435\u043C\u0432\u0440\u0438'],
    monthNamesShort: ['\u042F\u043D\u0443','\u0424\u0435\u0432','\u041C\u0430\u0440','\u0410\u043F\u0440','\u041C\u0430\u0439','\u042E\u043D\u0438',
    '\u042E\u043B\u0438','\u0410\u0432\u0433','\u0421\u0435\u043F','\u041E\u043A\u0442','\u041D\u043E\u0432','\u0414\u0435\u043A'],
    dayNames: ['\u041D\u0435\u0434\u0435\u043B\u044F','\u041F\u043E\u043D\u0435\u0434\u0435\u043B\u043D\u0438\u043A','\u0412\u0442\u043E\u0440\u043D\u0438\u043A','\u0421\u0440\u044F\u0434\u0430','\u0427\u0435\u0442\u0432\u044A\u0440\u0442\u044A\u043A','\u041F\u0435\u0442\u044A\u043A','\u0421\u044A\u0431\u043E\u0442\u0430'],
    dayNamesShort: ['\u041D\u0435\u0434','\u041F\u043E\u043D','\u0412\u0442\u043E','\u0421\u0440\u044F','\u0427\u0435\u0442','\u041F\u0435\u0442','\u0421\u044A\u0431'],
    dayNamesMin: ['\u041D\u0435','\u041F\u043E','\u0412\u0442','\u0421\u0440','\u0427\u0435','\u041F\u0435','\u0421\u044A'],
    weekHeader: 'Wk',
    dateFormat: 'dd.mm.yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['bg']);

var i18nDatepickerBg = datepicker.regional['bg'];


/* Bosnian i18n for the jQuery UI date picker plugin. */
/* Written by Kenan Konjo. */


datepicker.regional['bs'] = {
    closeText: 'Zatvori',
    prevText: '&#x3C;',
    nextText: '&#x3E;',
    currentText: 'Danas',
    monthNames: ['Januar','Februar','Mart','April','Maj','Juni',
    'Juli','August','Septembar','Oktobar','Novembar','Decembar'],
    monthNamesShort: ['Jan','Feb','Mar','Apr','Maj','Jun',
    'Jul','Aug','Sep','Okt','Nov','Dec'],
    dayNames: ['Nedelja','Ponedeljak','Utorak','Srijeda','\u010Cetvrtak','Petak','Subota'],
    dayNamesShort: ['Ned','Pon','Uto','Sri','\u010Cet','Pet','Sub'],
    dayNamesMin: ['Ne','Po','Ut','Sr','\u010Ce','Pe','Su'],
    weekHeader: 'Wk',
    dateFormat: 'dd.mm.yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['bs']);

var i18nDatepickerBs = datepicker.regional['bs'];


/* Inicialitzaci\u00F3 en catal\u00E0 per a l'extensi\u00F3 'UI date picker' per jQuery. */
/* Writers: (joan.leon@gmail.com). */


datepicker.regional['ca'] = {
    closeText: 'Tanca',
    prevText: 'Anterior',
    nextText: 'Seg\u00FCent',
    currentText: 'Avui',
    monthNames: ['gener','febrer','mar\u00E7','abril','maig','juny',
    'juliol','agost','setembre','octubre','novembre','desembre'],
    monthNamesShort: ['gen','feb','mar\u00E7','abr','maig','juny',
    'jul','ag','set','oct','nov','des'],
    dayNames: ['diumenge','dilluns','dimarts','dimecres','dijous','divendres','dissabte'],
    dayNamesShort: ['dg','dl','dt','dc','dj','dv','ds'],
    dayNamesMin: ['dg','dl','dt','dc','dj','dv','ds'],
    weekHeader: 'Set',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['ca']);

var i18nDatepickerCa = datepicker.regional['ca'];


/* Czech initialisation for the jQuery UI date picker plugin. */
/* Written by Tomas Muller (tomas@tomas-muller.net). */


datepicker.regional['cs'] = {
    closeText: 'Zav\u0159\u00EDt',
    prevText: '&#x3C;D\u0159\u00EDve',
    nextText: 'Pozd\u011Bji&#x3E;',
    currentText: 'Nyn\u00ED',
    monthNames: ['leden','\u00FAnor','b\u0159ezen','duben','kv\u011Bten','\u010Derven',
    '\u010Dervenec','srpen','z\u00E1\u0159\u00ED','\u0159\u00EDjen','listopad','prosinec'],
    monthNamesShort: ['led','\u00FAno','b\u0159e','dub','kv\u011B','\u010Der',
    '\u010Dvc','srp','z\u00E1\u0159','\u0159\u00EDj','lis','pro'],
    dayNames: ['ned\u011Ble', 'pond\u011Bl\u00ED', '\u00FAter\u00FD', 'st\u0159eda', '\u010Dtvrtek', 'p\u00E1tek', 'sobota'],
    dayNamesShort: ['ne', 'po', '\u00FAt', 'st', '\u010Dt', 'p\u00E1', 'so'],
    dayNamesMin: ['ne','po','\u00FAt','st','\u010Dt','p\u00E1','so'],
    weekHeader: 'T\u00FDd',
    dateFormat: 'dd.mm.yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['cs']);

var i18nDatepickerCs = datepicker.regional['cs'];


/* Welsh/UK initialisation for the jQuery UI date picker plugin. */
/* Written by William Griffiths. */


datepicker.regional['cy-GB'] = {
    closeText: 'Done',
    prevText: 'Prev',
    nextText: 'Next',
    currentText: 'Today',
    monthNames: ['Ionawr','Chwefror','Mawrth','Ebrill','Mai','Mehefin',
    'Gorffennaf','Awst','Medi','Hydref','Tachwedd','Rhagfyr'],
    monthNamesShort: ['Ion', 'Chw', 'Maw', 'Ebr', 'Mai', 'Meh',
    'Gor', 'Aws', 'Med', 'Hyd', 'Tac', 'Rha'],
    dayNames: ['Dydd Sul', 'Dydd Llun', 'Dydd Mawrth', 'Dydd Mercher', 'Dydd Iau', 'Dydd Gwener', 'Dydd Sadwrn'],
    dayNamesShort: ['Sul', 'Llu', 'Maw', 'Mer', 'Iau', 'Gwe', 'Sad'],
    dayNamesMin: ['Su','Ll','Ma','Me','Ia','Gw','Sa'],
    weekHeader: 'Wy',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['cy-GB']);

var i18nDatepickerCyGb = datepicker.regional['cy-GB'];


/* Danish initialisation for the jQuery UI date picker plugin. */
/* Written by Jan Christensen ( deletestuff@gmail.com). */


datepicker.regional['da'] = {
    closeText: 'Luk',
    prevText: '&#x3C;Forrige',
    nextText: 'N\u00E6ste&#x3E;',
    currentText: 'Idag',
    monthNames: ['Januar','Februar','Marts','April','Maj','Juni',
    'Juli','August','September','Oktober','November','December'],
    monthNamesShort: ['Jan','Feb','Mar','Apr','Maj','Jun',
    'Jul','Aug','Sep','Okt','Nov','Dec'],
    dayNames: ['S\u00F8ndag','Mandag','Tirsdag','Onsdag','Torsdag','Fredag','L\u00F8rdag'],
    dayNamesShort: ['S\u00F8n','Man','Tir','Ons','Tor','Fre','L\u00F8r'],
    dayNamesMin: ['S\u00F8','Ma','Ti','On','To','Fr','L\u00F8'],
    weekHeader: 'Uge',
    dateFormat: 'dd-mm-yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['da']);

var i18nDatepickerDa = datepicker.regional['da'];


/* German initialisation for the jQuery UI date picker plugin. */
/* Written by Milian Wolff (mail@milianw.de). */


datepicker.regional['de'] = {
    closeText: 'Schlie\u00DFen',
    prevText: '&#x3C;Zur\u00FCck',
    nextText: 'Vor&#x3E;',
    currentText: 'Heute',
    monthNames: ['Januar','Februar','M\u00E4rz','April','Mai','Juni',
    'Juli','August','September','Oktober','November','Dezember'],
    monthNamesShort: ['Jan','Feb','M\u00E4r','Apr','Mai','Jun',
    'Jul','Aug','Sep','Okt','Nov','Dez'],
    dayNames: ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'],
    dayNamesShort: ['So','Mo','Di','Mi','Do','Fr','Sa'],
    dayNamesMin: ['So','Mo','Di','Mi','Do','Fr','Sa'],
    weekHeader: 'KW',
    dateFormat: 'dd.mm.yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['de']);

var i18nDatepickerDe = datepicker.regional['de'];


/* Greek (el) initialisation for the jQuery UI date picker plugin. */
/* Written by Alex Cicovic (http://www.alexcicovic.com) */


datepicker.regional['el'] = {
    closeText: '\u039A\u03BB\u03B5\u03AF\u03C3\u03B9\u03BC\u03BF',
    prevText: '\u03A0\u03C1\u03BF\u03B7\u03B3\u03BF\u03CD\u03BC\u03B5\u03BD\u03BF\u03C2',
    nextText: '\u0395\u03C0\u03CC\u03BC\u03B5\u03BD\u03BF\u03C2',
    currentText: '\u03A3\u03AE\u03BC\u03B5\u03C1\u03B1',
    monthNames: ['\u0399\u03B1\u03BD\u03BF\u03C5\u03AC\u03C1\u03B9\u03BF\u03C2','\u03A6\u03B5\u03B2\u03C1\u03BF\u03C5\u03AC\u03C1\u03B9\u03BF\u03C2','\u039C\u03AC\u03C1\u03C4\u03B9\u03BF\u03C2','\u0391\u03C0\u03C1\u03AF\u03BB\u03B9\u03BF\u03C2','\u039C\u03AC\u03B9\u03BF\u03C2','\u0399\u03BF\u03CD\u03BD\u03B9\u03BF\u03C2',
    '\u0399\u03BF\u03CD\u03BB\u03B9\u03BF\u03C2','\u0391\u03CD\u03B3\u03BF\u03C5\u03C3\u03C4\u03BF\u03C2','\u03A3\u03B5\u03C0\u03C4\u03AD\u03BC\u03B2\u03C1\u03B9\u03BF\u03C2','\u039F\u03BA\u03C4\u03CE\u03B2\u03C1\u03B9\u03BF\u03C2','\u039D\u03BF\u03AD\u03BC\u03B2\u03C1\u03B9\u03BF\u03C2','\u0394\u03B5\u03BA\u03AD\u03BC\u03B2\u03C1\u03B9\u03BF\u03C2'],
    monthNamesShort: ['\u0399\u03B1\u03BD','\u03A6\u03B5\u03B2','\u039C\u03B1\u03C1','\u0391\u03C0\u03C1','\u039C\u03B1\u03B9','\u0399\u03BF\u03C5\u03BD',
    '\u0399\u03BF\u03C5\u03BB','\u0391\u03C5\u03B3','\u03A3\u03B5\u03C0','\u039F\u03BA\u03C4','\u039D\u03BF\u03B5','\u0394\u03B5\u03BA'],
    dayNames: ['\u039A\u03C5\u03C1\u03B9\u03B1\u03BA\u03AE','\u0394\u03B5\u03C5\u03C4\u03AD\u03C1\u03B1','\u03A4\u03C1\u03AF\u03C4\u03B7','\u03A4\u03B5\u03C4\u03AC\u03C1\u03C4\u03B7','\u03A0\u03AD\u03BC\u03C0\u03C4\u03B7','\u03A0\u03B1\u03C1\u03B1\u03C3\u03BA\u03B5\u03C5\u03AE','\u03A3\u03AC\u03B2\u03B2\u03B1\u03C4\u03BF'],
    dayNamesShort: ['\u039A\u03C5\u03C1','\u0394\u03B5\u03C5','\u03A4\u03C1\u03B9','\u03A4\u03B5\u03C4','\u03A0\u03B5\u03BC','\u03A0\u03B1\u03C1','\u03A3\u03B1\u03B2'],
    dayNamesMin: ['\u039A\u03C5','\u0394\u03B5','\u03A4\u03C1','\u03A4\u03B5','\u03A0\u03B5','\u03A0\u03B1','\u03A3\u03B1'],
    weekHeader: '\u0395\u03B2\u03B4',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['el']);

var i18nDatepickerEl = datepicker.regional['el'];


/* English/Australia initialisation for the jQuery UI date picker plugin. */
/* Based on the en-GB initialisation. */


datepicker.regional['en-AU'] = {
    closeText: 'Done',
    prevText: 'Prev',
    nextText: 'Next',
    currentText: 'Today',
    monthNames: ['January','February','March','April','May','June',
    'July','August','September','October','November','December'],
    monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
    dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
    dayNamesMin: ['Su','Mo','Tu','We','Th','Fr','Sa'],
    weekHeader: 'Wk',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['en-AU']);

var i18nDatepickerEnAu = datepicker.regional['en-AU'];


/* English/UK initialisation for the jQuery UI date picker plugin. */
/* Written by Stuart. */


datepicker.regional['en-GB'] = {
    closeText: 'Done',
    prevText: 'Prev',
    nextText: 'Next',
    currentText: 'Today',
    monthNames: ['January','February','March','April','May','June',
    'July','August','September','October','November','December'],
    monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
    dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
    dayNamesMin: ['Su','Mo','Tu','We','Th','Fr','Sa'],
    weekHeader: 'Wk',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['en-GB']);

var i18nDatepickerEnGb = datepicker.regional['en-GB'];


/* English/New Zealand initialisation for the jQuery UI date picker plugin. */
/* Based on the en-GB initialisation. */


datepicker.regional['en-NZ'] = {
    closeText: 'Done',
    prevText: 'Prev',
    nextText: 'Next',
    currentText: 'Today',
    monthNames: ['January','February','March','April','May','June',
    'July','August','September','October','November','December'],
    monthNamesShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    dayNames: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
    dayNamesShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
    dayNamesMin: ['Su','Mo','Tu','We','Th','Fr','Sa'],
    weekHeader: 'Wk',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['en-NZ']);

var i18nDatepickerEnNz = datepicker.regional['en-NZ'];


/* Esperanto initialisation for the jQuery UI date picker plugin. */
/* Written by Olivier M. (olivierweb@ifrance.com). */


datepicker.regional['eo'] = {
    closeText: 'Fermi',
    prevText: '&#x3C;Anta',
    nextText: 'Sekv&#x3E;',
    currentText: 'Nuna',
    monthNames: ['Januaro','Februaro','Marto','Aprilo','Majo','Junio',
    'Julio','A\u016Dgusto','Septembro','Oktobro','Novembro','Decembro'],
    monthNamesShort: ['Jan','Feb','Mar','Apr','Maj','Jun',
    'Jul','A\u016Dg','Sep','Okt','Nov','Dec'],
    dayNames: ['Diman\u0109o','Lundo','Mardo','Merkredo','\u0134a\u016Ddo','Vendredo','Sabato'],
    dayNamesShort: ['Dim','Lun','Mar','Mer','\u0134a\u016D','Ven','Sab'],
    dayNamesMin: ['Di','Lu','Ma','Me','\u0134a','Ve','Sa'],
    weekHeader: 'Sb',
    dateFormat: 'dd/mm/yy',
    firstDay: 0,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['eo']);

var i18nDatepickerEo = datepicker.regional['eo'];


/* Inicializaci\u00F3n en espa\u00F1ol para la extensi\u00F3n 'UI date picker' para jQuery. */
/* Traducido por Vester (xvester@gmail.com). */


datepicker.regional['es'] = {
    closeText: 'Cerrar',
    prevText: '&#x3C;Ant',
    nextText: 'Sig&#x3E;',
    currentText: 'Hoy',
    monthNames: ['enero','febrero','marzo','abril','mayo','junio',
    'julio','agosto','septiembre','octubre','noviembre','diciembre'],
    monthNamesShort: ['ene','feb','mar','abr','may','jun',
    'jul','ago','sep','oct','nov','dic'],
    dayNames: ['domingo','lunes','martes','mi\u00E9rcoles','jueves','viernes','s\u00E1bado'],
    dayNamesShort: ['dom','lun','mar','mi\u00E9','jue','vie','s\u00E1b'],
    dayNamesMin: ['D','L','M','X','J','V','S'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['es']);

var i18nDatepickerEs = datepicker.regional['es'];


/* Estonian initialisation for the jQuery UI date picker plugin. */
/* Written by Mart S\u00F5mermaa (mrts.pydev at gmail com). */


datepicker.regional['et'] = {
    closeText: 'Sulge',
    prevText: 'Eelnev',
    nextText: 'J\u00E4rgnev',
    currentText: 'T\u00E4na',
    monthNames: ['Jaanuar','Veebruar','M\u00E4rts','Aprill','Mai','Juuni',
    'Juuli','August','September','Oktoober','November','Detsember'],
    monthNamesShort: ['Jaan', 'Veebr', 'M\u00E4rts', 'Apr', 'Mai', 'Juuni',
    'Juuli', 'Aug', 'Sept', 'Okt', 'Nov', 'Dets'],
    dayNames: ['P\u00FChap\u00E4ev', 'Esmasp\u00E4ev', 'Teisip\u00E4ev', 'Kolmap\u00E4ev', 'Neljap\u00E4ev', 'Reede', 'Laup\u00E4ev'],
    dayNamesShort: ['P\u00FChap', 'Esmasp', 'Teisip', 'Kolmap', 'Neljap', 'Reede', 'Laup'],
    dayNamesMin: ['P','E','T','K','N','R','L'],
    weekHeader: 'n\u00E4d',
    dateFormat: 'dd.mm.yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['et']);

var i18nDatepickerEt = datepicker.regional['et'];


/* Karrikas-ek itzulia (karrikas@karrikas.com) */


datepicker.regional['eu'] = {
    closeText: 'Egina',
    prevText: '&#x3C;Aur',
    nextText: 'Hur&#x3E;',
    currentText: 'Gaur',
    monthNames: ['urtarrila','otsaila','martxoa','apirila','maiatza','ekaina',
        'uztaila','abuztua','iraila','urria','azaroa','abendua'],
    monthNamesShort: ['urt.','ots.','mar.','api.','mai.','eka.',
        'uzt.','abu.','ira.','urr.','aza.','abe.'],
    dayNames: ['igandea','astelehena','asteartea','asteazkena','osteguna','ostirala','larunbata'],
    dayNamesShort: ['ig.','al.','ar.','az.','og.','ol.','lr.'],
    dayNamesMin: ['ig','al','ar','az','og','ol','lr'],
    weekHeader: 'As',
    dateFormat: 'yy-mm-dd',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['eu']);

var i18nDatepickerEu = datepicker.regional['eu'];


/* Persian (Farsi) Translation for the jQuery UI date picker plugin. */
/* Javad Mowlanezhad -- jmowla@gmail.com */
/* Jalali calendar should supported soon! (Its implemented but I have to test it) */


datepicker.regional['fa'] = {
    closeText: '\u0628\u0633\u062A\u0646',
    prevText: '&#x3C;\u0642\u0628\u0644\u06CC',
    nextText: '\u0628\u0639\u062F\u06CC&#x3E;',
    currentText: '\u0627\u0645\u0631\u0648\u0632',
    monthNames: [
        '\u0698\u0627\u0646\u0648\u06CC\u0647',
        '\u0641\u0648\u0631\u06CC\u0647',
        '\u0645\u0627\u0631\u0633',
        '\u0622\u0648\u0631\u06CC\u0644',
        '\u0645\u0647',
        '\u0698\u0648\u0626\u0646',
        '\u0698\u0648\u0626\u06CC\u0647',
        '\u0627\u0648\u062A',
        '\u0633\u067E\u062A\u0627\u0645\u0628\u0631',
        '\u0627\u06A9\u062A\u0628\u0631',
        '\u0646\u0648\u0627\u0645\u0628\u0631',
        '\u062F\u0633\u0627\u0645\u0628\u0631'
    ],
    monthNamesShort: ['1','2','3','4','5','6','7','8','9','10','11','12'],
    dayNames: [
        '\u064A\u06A9\u0634\u0646\u0628\u0647',
        '\u062F\u0648\u0634\u0646\u0628\u0647',
        '\u0633\u0647\u200C\u0634\u0646\u0628\u0647',
        '\u0686\u0647\u0627\u0631\u0634\u0646\u0628\u0647',
        '\u067E\u0646\u062C\u0634\u0646\u0628\u0647',
        '\u062C\u0645\u0639\u0647',
        '\u0634\u0646\u0628\u0647'
    ],
    dayNamesShort: [
        '\u06CC',
        '\u062F',
        '\u0633',
        '\u0686',
        '\u067E',
        '\u062C',
        '\u0634'
    ],
    dayNamesMin: [
        '\u06CC',
        '\u062F',
        '\u0633',
        '\u0686',
        '\u067E',
        '\u062C',
        '\u0634'
    ],
    weekHeader: '\u0647\u0641',
    dateFormat: 'yy/mm/dd',
    firstDay: 6,
    isRTL: true,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['fa']);

var i18nDatepickerFa = datepicker.regional['fa'];


/* Finnish initialisation for the jQuery UI date picker plugin. */
/* Written by Harri Kilpi\u00F6 (harrikilpio@gmail.com). */


datepicker.regional['fi'] = {
    closeText: 'Sulje',
    prevText: '&#xAB;Edellinen',
    nextText: 'Seuraava&#xBB;',
    currentText: 'T\u00E4n\u00E4\u00E4n',
    monthNames: ['Tammikuu','Helmikuu','Maaliskuu','Huhtikuu','Toukokuu','Kes\u00E4kuu',
    'Hein\u00E4kuu','Elokuu','Syyskuu','Lokakuu','Marraskuu','Joulukuu'],
    monthNamesShort: ['Tammi','Helmi','Maalis','Huhti','Touko','Kes\u00E4',
    'Hein\u00E4','Elo','Syys','Loka','Marras','Joulu'],
    dayNamesShort: ['Su','Ma','Ti','Ke','To','Pe','La'],
    dayNames: ['Sunnuntai','Maanantai','Tiistai','Keskiviikko','Torstai','Perjantai','Lauantai'],
    dayNamesMin: ['Su','Ma','Ti','Ke','To','Pe','La'],
    weekHeader: 'Vk',
    dateFormat: 'd.m.yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['fi']);

var i18nDatepickerFi = datepicker.regional['fi'];


/* Faroese initialisation for the jQuery UI date picker plugin */
/* Written by Sverri Mohr Olsen, sverrimo@gmail.com */


datepicker.regional['fo'] = {
    closeText: 'Lat aftur',
    prevText: '&#x3C;Fyrra',
    nextText: 'N\u00E6sta&#x3E;',
    currentText: '\u00CD dag',
    monthNames: ['Januar','Februar','Mars','Apr\u00EDl','Mei','Juni',
    'Juli','August','September','Oktober','November','Desember'],
    monthNamesShort: ['Jan','Feb','Mar','Apr','Mei','Jun',
    'Jul','Aug','Sep','Okt','Nov','Des'],
    dayNames: ['Sunnudagur','M\u00E1nadagur','T\u00FDsdagur','Mikudagur','H\u00F3sdagur','Fr\u00EDggjadagur','Leyardagur'],
    dayNamesShort: ['Sun','M\u00E1n','T\u00FDs','Mik','H\u00F3s','Fr\u00ED','Ley'],
    dayNamesMin: ['Su','M\u00E1','T\u00FD','Mi','H\u00F3','Fr','Le'],
    weekHeader: 'Vk',
    dateFormat: 'dd-mm-yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['fo']);

var i18nDatepickerFo = datepicker.regional['fo'];


/* Canadian-French initialisation for the jQuery UI date picker plugin. */


datepicker.regional['fr-CA'] = {
    closeText: 'Fermer',
    prevText: 'Pr\u00E9c\u00E9dent',
    nextText: 'Suivant',
    currentText: 'Aujourd\'hui',
    monthNames: ['janvier', 'f\u00E9vrier', 'mars', 'avril', 'mai', 'juin',
        'juillet', 'ao\u00FBt', 'septembre', 'octobre', 'novembre', 'd\u00E9cembre'],
    monthNamesShort: ['janv.', 'f\u00E9vr.', 'mars', 'avril', 'mai', 'juin',
        'juil.', 'ao\u00FBt', 'sept.', 'oct.', 'nov.', 'd\u00E9c.'],
    dayNames: ['dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'],
    dayNamesShort: ['dim.', 'lun.', 'mar.', 'mer.', 'jeu.', 'ven.', 'sam.'],
    dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
    weekHeader: 'Sem.',
    dateFormat: 'yy-mm-dd',
    firstDay: 0,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''
};
datepicker.setDefaults(datepicker.regional['fr-CA']);

var i18nDatepickerFrCa = datepicker.regional['fr-CA'];


/* Swiss-French initialisation for the jQuery UI date picker plugin. */
/* Written Martin Voelkle (martin.voelkle@e-tc.ch). */


datepicker.regional['fr-CH'] = {
    closeText: 'Fermer',
    prevText: '&#x3C;Pr\u00E9c',
    nextText: 'Suiv&#x3E;',
    currentText: 'Courant',
    monthNames: ['janvier', 'f\u00E9vrier', 'mars', 'avril', 'mai', 'juin',
        'juillet', 'ao\u00FBt', 'septembre', 'octobre', 'novembre', 'd\u00E9cembre'],
    monthNamesShort: ['janv.', 'f\u00E9vr.', 'mars', 'avril', 'mai', 'juin',
        'juil.', 'ao\u00FBt', 'sept.', 'oct.', 'nov.', 'd\u00E9c.'],
    dayNames: ['dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'],
    dayNamesShort: ['dim.', 'lun.', 'mar.', 'mer.', 'jeu.', 'ven.', 'sam.'],
    dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
    weekHeader: 'Sm',
    dateFormat: 'dd.mm.yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['fr-CH']);

var i18nDatepickerFrCh = datepicker.regional['fr-CH'];


/* French initialisation for the jQuery UI date picker plugin. */
/* Written by Keith Wood (kbwood{at}iinet.com.au),
              St\u00E9phane Nahmani (sholby@sholby.net),
              St\u00E9phane Raimbault <stephane.raimbault@gmail.com> */


datepicker.regional['fr'] = {
    closeText: 'Fermer',
    prevText: 'Pr\u00E9c\u00E9dent',
    nextText: 'Suivant',
    currentText: 'Aujourd\'hui',
    monthNames: ['janvier', 'f\u00E9vrier', 'mars', 'avril', 'mai', 'juin',
        'juillet', 'ao\u00FBt', 'septembre', 'octobre', 'novembre', 'd\u00E9cembre'],
    monthNamesShort: ['janv.', 'f\u00E9vr.', 'mars', 'avr.', 'mai', 'juin',
        'juil.', 'ao\u00FBt', 'sept.', 'oct.', 'nov.', 'd\u00E9c.'],
    dayNames: ['dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi'],
    dayNamesShort: ['dim.', 'lun.', 'mar.', 'mer.', 'jeu.', 'ven.', 'sam.'],
    dayNamesMin: ['D','L','M','M','J','V','S'],
    weekHeader: 'Sem.',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['fr']);

var i18nDatepickerFr = datepicker.regional['fr'];


/* Galician localization for 'UI date picker' jQuery extension. */
/* Translated by Jorge Barreiro <yortx.barry@gmail.com>. */


datepicker.regional['gl'] = {
    closeText: 'Pechar',
    prevText: '&#x3C;Ant',
    nextText: 'Seg&#x3E;',
    currentText: 'Hoxe',
    monthNames: ['Xaneiro','Febreiro','Marzo','Abril','Maio','Xu\u00F1o',
    'Xullo','Agosto','Setembro','Outubro','Novembro','Decembro'],
    monthNamesShort: ['Xan','Feb','Mar','Abr','Mai','Xu\u00F1',
    'Xul','Ago','Set','Out','Nov','Dec'],
    dayNames: ['Domingo','Luns','Martes','M\u00E9rcores','Xoves','Venres','S\u00E1bado'],
    dayNamesShort: ['Dom','Lun','Mar','M\u00E9r','Xov','Ven','S\u00E1b'],
    dayNamesMin: ['Do','Lu','Ma','M\u00E9','Xo','Ve','S\u00E1'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['gl']);

var i18nDatepickerGl = datepicker.regional['gl'];


/* Hebrew initialisation for the UI Datepicker extension. */
/* Written by Amir Hardon (ahardon at gmail dot com). */


datepicker.regional['he'] = {
    closeText: '\u05E1\u05D2\u05D5\u05E8',
    prevText: '&#x3C;\u05D4\u05E7\u05D5\u05D3\u05DD',
    nextText: '\u05D4\u05D1\u05D0&#x3E;',
    currentText: '\u05D4\u05D9\u05D5\u05DD',
    monthNames: ['\u05D9\u05E0\u05D5\u05D0\u05E8','\u05E4\u05D1\u05E8\u05D5\u05D0\u05E8','\u05DE\u05E8\u05E5','\u05D0\u05E4\u05E8\u05D9\u05DC','\u05DE\u05D0\u05D9','\u05D9\u05D5\u05E0\u05D9',
    '\u05D9\u05D5\u05DC\u05D9','\u05D0\u05D5\u05D2\u05D5\u05E1\u05D8','\u05E1\u05E4\u05D8\u05DE\u05D1\u05E8','\u05D0\u05D5\u05E7\u05D8\u05D5\u05D1\u05E8','\u05E0\u05D5\u05D1\u05DE\u05D1\u05E8','\u05D3\u05E6\u05DE\u05D1\u05E8'],
    monthNamesShort: ['\u05D9\u05E0\u05D5','\u05E4\u05D1\u05E8','\u05DE\u05E8\u05E5','\u05D0\u05E4\u05E8','\u05DE\u05D0\u05D9','\u05D9\u05D5\u05E0\u05D9',
    '\u05D9\u05D5\u05DC\u05D9','\u05D0\u05D5\u05D2','\u05E1\u05E4\u05D8','\u05D0\u05D5\u05E7','\u05E0\u05D5\u05D1','\u05D3\u05E6\u05DE'],
    dayNames: ['\u05E8\u05D0\u05E9\u05D5\u05DF','\u05E9\u05E0\u05D9','\u05E9\u05DC\u05D9\u05E9\u05D9','\u05E8\u05D1\u05D9\u05E2\u05D9','\u05D7\u05DE\u05D9\u05E9\u05D9','\u05E9\u05D9\u05E9\u05D9','\u05E9\u05D1\u05EA'],
    dayNamesShort: ['\u05D0\'','\u05D1\'','\u05D2\'','\u05D3\'','\u05D4\'','\u05D5\'','\u05E9\u05D1\u05EA'],
    dayNamesMin: ['\u05D0\'','\u05D1\'','\u05D2\'','\u05D3\'','\u05D4\'','\u05D5\'','\u05E9\u05D1\u05EA'],
    weekHeader: 'Wk',
    dateFormat: 'dd/mm/yy',
    firstDay: 0,
    isRTL: true,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['he']);

var i18nDatepickerHe = datepicker.regional['he'];


/* Hindi initialisation for the jQuery UI date picker plugin. */
/* Written by Michael Dawart. */


datepicker.regional['hi'] = {
    closeText: '\u092C\u0902\u0926',
    prevText: '\u092A\u093F\u091B\u0932\u093E',
    nextText: '\u0905\u0917\u0932\u093E',
    currentText: '\u0906\u091C',
    monthNames: ['\u091C\u0928\u0935\u0930\u0940 ','\u092B\u0930\u0935\u0930\u0940','\u092E\u093E\u0930\u094D\u091A','\u0905\u092A\u094D\u0930\u0947\u0932','\u092E\u0908','\u091C\u0942\u0928',
    '\u091C\u0942\u0932\u093E\u0908','\u0905\u0917\u0938\u094D\u0924 ','\u0938\u093F\u0924\u092E\u094D\u092C\u0930','\u0905\u0915\u094D\u091F\u0942\u092C\u0930','\u0928\u0935\u092E\u094D\u092C\u0930','\u0926\u093F\u0938\u092E\u094D\u092C\u0930'],
    monthNamesShort: ['\u091C\u0928', '\u092B\u0930', '\u092E\u093E\u0930\u094D\u091A', '\u0905\u092A\u094D\u0930\u0947\u0932', '\u092E\u0908', '\u091C\u0942\u0928',
    '\u091C\u0942\u0932\u093E\u0908', '\u0905\u0917', '\u0938\u093F\u0924', '\u0905\u0915\u094D\u091F', '\u0928\u0935', '\u0926\u093F'],
    dayNames: ['\u0930\u0935\u093F\u0935\u093E\u0930', '\u0938\u094B\u092E\u0935\u093E\u0930', '\u092E\u0902\u0917\u0932\u0935\u093E\u0930', '\u092C\u0941\u0927\u0935\u093E\u0930', '\u0917\u0941\u0930\u0941\u0935\u093E\u0930', '\u0936\u0941\u0915\u094D\u0930\u0935\u093E\u0930', '\u0936\u0928\u093F\u0935\u093E\u0930'],
    dayNamesShort: ['\u0930\u0935\u093F', '\u0938\u094B\u092E', '\u092E\u0902\u0917\u0932', '\u092C\u0941\u0927', '\u0917\u0941\u0930\u0941', '\u0936\u0941\u0915\u094D\u0930', '\u0936\u0928\u093F'],
    dayNamesMin: ['\u0930\u0935\u093F', '\u0938\u094B\u092E', '\u092E\u0902\u0917\u0932', '\u092C\u0941\u0927', '\u0917\u0941\u0930\u0941', '\u0936\u0941\u0915\u094D\u0930', '\u0936\u0928\u093F'],
    weekHeader: '\u0939\u092B\u094D\u0924\u093E',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['hi']);

var i18nDatepickerHi = datepicker.regional['hi'];


/* Croatian i18n for the jQuery UI date picker plugin. */
/* Written by Vjekoslav Nesek. */


datepicker.regional['hr'] = {
    closeText: 'Zatvori',
    prevText: '&#x3C;',
    nextText: '&#x3E;',
    currentText: 'Danas',
    monthNames: ['Sije\u010Danj','Velja\u010Da','O\u017Eujak','Travanj','Svibanj','Lipanj',
    'Srpanj','Kolovoz','Rujan','Listopad','Studeni','Prosinac'],
    monthNamesShort: ['Sij','Velj','O\u017Eu','Tra','Svi','Lip',
    'Srp','Kol','Ruj','Lis','Stu','Pro'],
    dayNames: ['Nedjelja','Ponedjeljak','Utorak','Srijeda','\u010Cetvrtak','Petak','Subota'],
    dayNamesShort: ['Ned','Pon','Uto','Sri','\u010Cet','Pet','Sub'],
    dayNamesMin: ['Ne','Po','Ut','Sr','\u010Ce','Pe','Su'],
    weekHeader: 'Tje',
    dateFormat: 'dd.mm.yy.',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['hr']);

var i18nDatepickerHr = datepicker.regional['hr'];


/* Hungarian initialisation for the jQuery UI date picker plugin. */


datepicker.regional['hu'] = {
    closeText: 'bez\u00E1r',
    prevText: 'vissza',
    nextText: 'el\u0151re',
    currentText: 'ma',
    monthNames: ['Janu\u00E1r', 'Febru\u00E1r', 'M\u00E1rcius', '\u00C1prilis', 'M\u00E1jus', 'J\u00FAnius',
    'J\u00FAlius', 'Augusztus', 'Szeptember', 'Okt\u00F3ber', 'November', 'December'],
    monthNamesShort: ['Jan', 'Feb', 'M\u00E1r', '\u00C1pr', 'M\u00E1j', 'J\u00FAn',
    'J\u00FAl', 'Aug', 'Szep', 'Okt', 'Nov', 'Dec'],
    dayNames: ['Vas\u00E1rnap', 'H\u00E9tf\u0151', 'Kedd', 'Szerda', 'Cs\u00FCt\u00F6rt\u00F6k', 'P\u00E9ntek', 'Szombat'],
    dayNamesShort: ['Vas', 'H\u00E9t', 'Ked', 'Sze', 'Cs\u00FC', 'P\u00E9n', 'Szo'],
    dayNamesMin: ['V', 'H', 'K', 'Sze', 'Cs', 'P', 'Szo'],
    weekHeader: 'H\u00E9t',
    dateFormat: 'yy.mm.dd.',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: true,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['hu']);

var i18nDatepickerHu = datepicker.regional['hu'];


/* Armenian(UTF-8) initialisation for the jQuery UI date picker plugin. */
/* Written by Levon Zakaryan (levon.zakaryan@gmail.com)*/


datepicker.regional['hy'] = {
    closeText: '\u0553\u0561\u056F\u0565\u056C',
    prevText: '&#x3C;\u0546\u0561\u056D.',
    nextText: '\u0540\u0561\u057B.&#x3E;',
    currentText: '\u0531\u0575\u057D\u0585\u0580',
    monthNames: ['\u0540\u0578\u0582\u0576\u057E\u0561\u0580','\u0553\u0565\u057F\u0580\u057E\u0561\u0580','\u0544\u0561\u0580\u057F','\u0531\u057A\u0580\u056B\u056C','\u0544\u0561\u0575\u056B\u057D','\u0540\u0578\u0582\u0576\u056B\u057D',
    '\u0540\u0578\u0582\u056C\u056B\u057D','\u0555\u0563\u0578\u057D\u057F\u0578\u057D','\u054D\u0565\u057A\u057F\u0565\u0574\u0562\u0565\u0580','\u0540\u0578\u056F\u057F\u0565\u0574\u0562\u0565\u0580','\u0546\u0578\u0575\u0565\u0574\u0562\u0565\u0580','\u0534\u0565\u056F\u057F\u0565\u0574\u0562\u0565\u0580'],
    monthNamesShort: ['\u0540\u0578\u0582\u0576\u057E','\u0553\u0565\u057F\u0580','\u0544\u0561\u0580\u057F','\u0531\u057A\u0580','\u0544\u0561\u0575\u056B\u057D','\u0540\u0578\u0582\u0576\u056B\u057D',
    '\u0540\u0578\u0582\u056C','\u0555\u0563\u057D','\u054D\u0565\u057A','\u0540\u0578\u056F','\u0546\u0578\u0575','\u0534\u0565\u056F'],
    dayNames: ['\u056F\u056B\u0580\u0561\u056F\u056B','\u0565\u056F\u0578\u0582\u0577\u0561\u0562\u0569\u056B','\u0565\u0580\u0565\u0584\u0577\u0561\u0562\u0569\u056B','\u0579\u0578\u0580\u0565\u0584\u0577\u0561\u0562\u0569\u056B','\u0570\u056B\u0576\u0563\u0577\u0561\u0562\u0569\u056B','\u0578\u0582\u0580\u0562\u0561\u0569','\u0577\u0561\u0562\u0561\u0569'],
    dayNamesShort: ['\u056F\u056B\u0580','\u0565\u0580\u056F','\u0565\u0580\u0584','\u0579\u0580\u0584','\u0570\u0576\u0563','\u0578\u0582\u0580\u0562','\u0577\u0562\u0569'],
    dayNamesMin: ['\u056F\u056B\u0580','\u0565\u0580\u056F','\u0565\u0580\u0584','\u0579\u0580\u0584','\u0570\u0576\u0563','\u0578\u0582\u0580\u0562','\u0577\u0562\u0569'],
    weekHeader: '\u0547\u0532\u054F',
    dateFormat: 'dd.mm.yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['hy']);

var i18nDatepickerHy = datepicker.regional['hy'];


/* Indonesian initialisation for the jQuery UI date picker plugin. */
/* Written by Deden Fathurahman (dedenf@gmail.com). */


datepicker.regional['id'] = {
    closeText: 'Tutup',
    prevText: '&#x3C;mundur',
    nextText: 'maju&#x3E;',
    currentText: 'hari ini',
    monthNames: ['Januari','Februari','Maret','April','Mei','Juni',
    'Juli','Agustus','September','Oktober','Nopember','Desember'],
    monthNamesShort: ['Jan','Feb','Mar','Apr','Mei','Jun',
    'Jul','Agus','Sep','Okt','Nop','Des'],
    dayNames: ['Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu'],
    dayNamesShort: ['Min','Sen','Sel','Rab','kam','Jum','Sab'],
    dayNamesMin: ['Mg','Sn','Sl','Rb','Km','jm','Sb'],
    weekHeader: 'Mg',
    dateFormat: 'dd/mm/yy',
    firstDay: 0,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['id']);

var i18nDatepickerId = datepicker.regional['id'];


/* Icelandic initialisation for the jQuery UI date picker plugin. */
/* Written by Haukur H. Thorsson (haukur@eskill.is). */


datepicker.regional['is'] = {
    closeText: 'Loka',
    prevText: '&#x3C; Fyrri',
    nextText: 'N\u00E6sti &#x3E;',
    currentText: '\u00CD dag',
    monthNames: ['Jan\u00FAar','Febr\u00FAar','Mars','Apr\u00EDl','Ma\u00ED','J\u00FAn\u00ED',
    'J\u00FAl\u00ED','\u00C1g\u00FAst','September','Okt\u00F3ber','N\u00F3vember','Desember'],
    monthNamesShort: ['Jan','Feb','Mar','Apr','Ma\u00ED','J\u00FAn',
    'J\u00FAl','\u00C1g\u00FA','Sep','Okt','N\u00F3v','Des'],
    dayNames: ['Sunnudagur','M\u00E1nudagur','\u00DEri\u00F0judagur','Mi\u00F0vikudagur','Fimmtudagur','F\u00F6studagur','Laugardagur'],
    dayNamesShort: ['Sun','M\u00E1n','\u00DEri','Mi\u00F0','Fim','F\u00F6s','Lau'],
    dayNamesMin: ['Su','M\u00E1','\u00DEr','Mi','Fi','F\u00F6','La'],
    weekHeader: 'Vika',
    dateFormat: 'dd.mm.yy',
    firstDay: 0,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['is']);

var i18nDatepickerIs = datepicker.regional['is'];


/* Italian initialisation for the jQuery UI date picker plugin. */
/* Written by Antonello Pasella (antonello.pasella@gmail.com). */


datepicker.regional['it-CH'] = {
    closeText: 'Chiudi',
    prevText: '&#x3C;Prec',
    nextText: 'Succ&#x3E;',
    currentText: 'Oggi',
    monthNames: ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno',
        'Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre'],
    monthNamesShort: ['Gen','Feb','Mar','Apr','Mag','Giu',
        'Lug','Ago','Set','Ott','Nov','Dic'],
    dayNames: ['Domenica','Luned\u00EC','Marted\u00EC','Mercoled\u00EC','Gioved\u00EC','Venerd\u00EC','Sabato'],
    dayNamesShort: ['Dom','Lun','Mar','Mer','Gio','Ven','Sab'],
    dayNamesMin: ['Do','Lu','Ma','Me','Gi','Ve','Sa'],
    weekHeader: 'Sm',
    dateFormat: 'dd.mm.yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['it-CH']);

var i18nDatepickerItCh = datepicker.regional['it-CH'];


/* Italian initialisation for the jQuery UI date picker plugin. */
/* Written by Antonello Pasella (antonello.pasella@gmail.com). */


datepicker.regional['it'] = {
    closeText: 'Chiudi',
    prevText: '&#x3C;Prec',
    nextText: 'Succ&#x3E;',
    currentText: 'Oggi',
    monthNames: ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno',
        'Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre'],
    monthNamesShort: ['Gen','Feb','Mar','Apr','Mag','Giu',
        'Lug','Ago','Set','Ott','Nov','Dic'],
    dayNames: ['Domenica','Luned\u00EC','Marted\u00EC','Mercoled\u00EC','Gioved\u00EC','Venerd\u00EC','Sabato'],
    dayNamesShort: ['Dom','Lun','Mar','Mer','Gio','Ven','Sab'],
    dayNamesMin: ['Do','Lu','Ma','Me','Gi','Ve','Sa'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['it']);

var i18nDatepickerIt = datepicker.regional['it'];


/* Japanese initialisation for the jQuery UI date picker plugin. */
/* Written by Kentaro SATO (kentaro@ranvis.com). */


datepicker.regional['ja'] = {
    closeText: '\u9589\u3058\u308B',
    prevText: '&#x3C;\u524D',
    nextText: '\u6B21&#x3E;',
    currentText: '\u4ECA\u65E5',
    monthNames: ['1\u6708','2\u6708','3\u6708','4\u6708','5\u6708','6\u6708',
    '7\u6708','8\u6708','9\u6708','10\u6708','11\u6708','12\u6708'],
    monthNamesShort: ['1\u6708','2\u6708','3\u6708','4\u6708','5\u6708','6\u6708',
    '7\u6708','8\u6708','9\u6708','10\u6708','11\u6708','12\u6708'],
    dayNames: ['\u65E5\u66DC\u65E5','\u6708\u66DC\u65E5','\u706B\u66DC\u65E5','\u6C34\u66DC\u65E5','\u6728\u66DC\u65E5','\u91D1\u66DC\u65E5','\u571F\u66DC\u65E5'],
    dayNamesShort: ['\u65E5','\u6708','\u706B','\u6C34','\u6728','\u91D1','\u571F'],
    dayNamesMin: ['\u65E5','\u6708','\u706B','\u6C34','\u6728','\u91D1','\u571F'],
    weekHeader: '\u9031',
    dateFormat: 'yy/mm/dd',
    firstDay: 0,
    isRTL: false,
    showMonthAfterYear: true,
    yearSuffix: '\u5E74'};
datepicker.setDefaults(datepicker.regional['ja']);

var i18nDatepickerJa = datepicker.regional['ja'];


/* Georgian (UTF-8) initialisation for the jQuery UI date picker plugin. */
/* Written by Lado Lomidze (lado.lomidze@gmail.com). */


datepicker.regional['ka'] = {
    closeText: '\u10D3\u10D0\u10EE\u10E3\u10E0\u10D5\u10D0',
    prevText: '&#x3c; \u10EC\u10D8\u10DC\u10D0',
    nextText: '\u10E8\u10D4\u10DB\u10D3\u10D4\u10D2\u10D8 &#x3e;',
    currentText: '\u10D3\u10E6\u10D4\u10E1',
    monthNames: ['\u10D8\u10D0\u10DC\u10D5\u10D0\u10E0\u10D8','\u10D7\u10D4\u10D1\u10D4\u10E0\u10D5\u10D0\u10DA\u10D8','\u10DB\u10D0\u10E0\u10E2\u10D8','\u10D0\u10DE\u10E0\u10D8\u10DA\u10D8','\u10DB\u10D0\u10D8\u10E1\u10D8','\u10D8\u10D5\u10DC\u10D8\u10E1\u10D8', '\u10D8\u10D5\u10DA\u10D8\u10E1\u10D8','\u10D0\u10D2\u10D5\u10D8\u10E1\u10E2\u10DD','\u10E1\u10D4\u10E5\u10E2\u10D4\u10DB\u10D1\u10D4\u10E0\u10D8','\u10DD\u10E5\u10E2\u10DD\u10DB\u10D1\u10D4\u10E0\u10D8','\u10DC\u10DD\u10D4\u10DB\u10D1\u10D4\u10E0\u10D8','\u10D3\u10D4\u10D9\u10D4\u10DB\u10D1\u10D4\u10E0\u10D8'],
    monthNamesShort: ['\u10D8\u10D0\u10DC','\u10D7\u10D4\u10D1','\u10DB\u10D0\u10E0','\u10D0\u10DE\u10E0','\u10DB\u10D0\u10D8','\u10D8\u10D5\u10DC', '\u10D8\u10D5\u10DA','\u10D0\u10D2\u10D5','\u10E1\u10D4\u10E5','\u10DD\u10E5\u10E2','\u10DC\u10DD\u10D4','\u10D3\u10D4\u10D9'],
    dayNames: ['\u10D9\u10D5\u10D8\u10E0\u10D0','\u10DD\u10E0\u10E8\u10D0\u10D1\u10D0\u10D7\u10D8','\u10E1\u10D0\u10DB\u10E8\u10D0\u10D1\u10D0\u10D7\u10D8','\u10DD\u10D7\u10EE\u10E8\u10D0\u10D1\u10D0\u10D7\u10D8','\u10EE\u10E3\u10D7\u10E8\u10D0\u10D1\u10D0\u10D7\u10D8','\u10DE\u10D0\u10E0\u10D0\u10E1\u10D9\u10D4\u10D5\u10D8','\u10E8\u10D0\u10D1\u10D0\u10D7\u10D8'],
    dayNamesShort: ['\u10D9\u10D5','\u10DD\u10E0\u10E8','\u10E1\u10D0\u10DB','\u10DD\u10D7\u10EE','\u10EE\u10E3\u10D7','\u10DE\u10D0\u10E0','\u10E8\u10D0\u10D1'],
    dayNamesMin: ['\u10D9\u10D5','\u10DD\u10E0\u10E8','\u10E1\u10D0\u10DB','\u10DD\u10D7\u10EE','\u10EE\u10E3\u10D7','\u10DE\u10D0\u10E0','\u10E8\u10D0\u10D1'],
    weekHeader: '\u10D9\u10D5\u10D8\u10E0\u10D0',
    dateFormat: 'dd-mm-yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['ka']);

var i18nDatepickerKa = datepicker.regional['ka'];


/* Kazakh (UTF-8) initialisation for the jQuery UI date picker plugin. */
/* Written by Dmitriy Karasyov (dmitriy.karasyov@gmail.com). */


datepicker.regional['kk'] = {
    closeText: '\u0416\u0430\u0431\u0443',
    prevText: '&#x3C;\u0410\u043B\u0434\u044B\u04A3\u0493\u044B',
    nextText: '\u041A\u0435\u043B\u0435\u0441\u0456&#x3E;',
    currentText: '\u0411\u04AF\u0433\u0456\u043D',
    monthNames: ['\u049A\u0430\u04A3\u0442\u0430\u0440','\u0410\u049B\u043F\u0430\u043D','\u041D\u0430\u0443\u0440\u044B\u0437','\u0421\u04D9\u0443\u0456\u0440','\u041C\u0430\u043C\u044B\u0440','\u041C\u0430\u0443\u0441\u044B\u043C',
    '\u0428\u0456\u043B\u0434\u0435','\u0422\u0430\u043C\u044B\u0437','\u049A\u044B\u0440\u043A\u04AF\u0439\u0435\u043A','\u049A\u0430\u0437\u0430\u043D','\u049A\u0430\u0440\u0430\u0448\u0430','\u0416\u0435\u043B\u0442\u043E\u049B\u0441\u0430\u043D'],
    monthNamesShort: ['\u049A\u0430\u04A3','\u0410\u049B\u043F','\u041D\u0430\u0443','\u0421\u04D9\u0443','\u041C\u0430\u043C','\u041C\u0430\u0443',
    '\u0428\u0456\u043B','\u0422\u0430\u043C','\u049A\u044B\u0440','\u049A\u0430\u0437','\u049A\u0430\u0440','\u0416\u0435\u043B'],
    dayNames: ['\u0416\u0435\u043A\u0441\u0435\u043D\u0431\u0456','\u0414\u04AF\u0439\u0441\u0435\u043D\u0431\u0456','\u0421\u0435\u0439\u0441\u0435\u043D\u0431\u0456','\u0421\u04D9\u0440\u0441\u0435\u043D\u0431\u0456','\u0411\u0435\u0439\u0441\u0435\u043D\u0431\u0456','\u0416\u04B1\u043C\u0430','\u0421\u0435\u043D\u0431\u0456'],
    dayNamesShort: ['\u0436\u043A\u0441','\u0434\u0441\u043D','\u0441\u0441\u043D','\u0441\u0440\u0441','\u0431\u0441\u043D','\u0436\u043C\u0430','\u0441\u043D\u0431'],
    dayNamesMin: ['\u0416\u043A','\u0414\u0441','\u0421\u0441','\u0421\u0440','\u0411\u0441','\u0416\u043C','\u0421\u043D'],
    weekHeader: '\u041D\u0435',
    dateFormat: 'dd.mm.yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['kk']);

var i18nDatepickerKk = datepicker.regional['kk'];


/* Khmer initialisation for the jQuery calendar extension. */
/* Written by Chandara Om (chandara.teacher@gmail.com). */


datepicker.regional['km'] = {
    closeText: '\u1792\u17D2\u179C\u17BE\u200B\u179A\u17BD\u1785',
    prevText: '\u1798\u17BB\u1793',
    nextText: '\u1794\u1793\u17D2\u1791\u17B6\u1794\u17CB',
    currentText: '\u1790\u17D2\u1784\u17C3\u200B\u1793\u17C1\u17C7',
    monthNames: ['\u1798\u1780\u179A\u17B6','\u1780\u17BB\u1798\u17D2\u1797\u17C8','\u1798\u17B8\u1793\u17B6','\u1798\u17C1\u179F\u17B6','\u17A7\u179F\u1797\u17B6','\u1798\u17B7\u1790\u17BB\u1793\u17B6',
    '\u1780\u1780\u17D2\u1780\u178A\u17B6','\u179F\u17B8\u17A0\u17B6','\u1780\u1789\u17D2\u1789\u17B6','\u178F\u17BB\u179B\u17B6','\u179C\u17B7\u1785\u17D2\u1786\u17B7\u1780\u17B6','\u1792\u17D2\u1793\u17BC'],
    monthNamesShort: ['\u1798\u1780\u179A\u17B6','\u1780\u17BB\u1798\u17D2\u1797\u17C8','\u1798\u17B8\u1793\u17B6','\u1798\u17C1\u179F\u17B6','\u17A7\u179F\u1797\u17B6','\u1798\u17B7\u1790\u17BB\u1793\u17B6',
    '\u1780\u1780\u17D2\u1780\u178A\u17B6','\u179F\u17B8\u17A0\u17B6','\u1780\u1789\u17D2\u1789\u17B6','\u178F\u17BB\u179B\u17B6','\u179C\u17B7\u1785\u17D2\u1786\u17B7\u1780\u17B6','\u1792\u17D2\u1793\u17BC'],
    dayNames: ['\u17A2\u17B6\u1791\u17B7\u178F\u17D2\u1799', '\u1785\u1793\u17D2\u1791', '\u17A2\u1784\u17D2\u1782\u17B6\u179A', '\u1796\u17BB\u1792', '\u1796\u17D2\u179A\u17A0\u179F\u17D2\u1794\u178F\u17B7\u17CD', '\u179F\u17BB\u1780\u17D2\u179A', '\u179F\u17C5\u179A\u17CD'],
    dayNamesShort: ['\u17A2\u17B6', '\u1785', '\u17A2', '\u1796\u17BB', '\u1796\u17D2\u179A\u17A0', '\u179F\u17BB', '\u179F\u17C5'],
    dayNamesMin: ['\u17A2\u17B6', '\u1785', '\u17A2', '\u1796\u17BB', '\u1796\u17D2\u179A\u17A0', '\u179F\u17BB', '\u179F\u17C5'],
    weekHeader: '\u179F\u1794\u17D2\u178A\u17B6\u17A0\u17CD',
    dateFormat: 'dd-mm-yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['km']);

var i18nDatepickerKm = datepicker.regional['km'];


/* Korean initialisation for the jQuery calendar extension. */
/* Written by DaeKwon Kang (ncrash.dk@gmail.com), Edited by Genie. */


datepicker.regional['ko'] = {
    closeText: '\uB2EB\uAE30',
    prevText: '\uC774\uC804\uB2EC',
    nextText: '\uB2E4\uC74C\uB2EC',
    currentText: '\uC624\uB298',
    monthNames: ['1\uC6D4','2\uC6D4','3\uC6D4','4\uC6D4','5\uC6D4','6\uC6D4',
    '7\uC6D4','8\uC6D4','9\uC6D4','10\uC6D4','11\uC6D4','12\uC6D4'],
    monthNamesShort: ['1\uC6D4','2\uC6D4','3\uC6D4','4\uC6D4','5\uC6D4','6\uC6D4',
    '7\uC6D4','8\uC6D4','9\uC6D4','10\uC6D4','11\uC6D4','12\uC6D4'],
    dayNames: ['\uC77C\uC694\uC77C','\uC6D4\uC694\uC77C','\uD654\uC694\uC77C','\uC218\uC694\uC77C','\uBAA9\uC694\uC77C','\uAE08\uC694\uC77C','\uD1A0\uC694\uC77C'],
    dayNamesShort: ['\uC77C','\uC6D4','\uD654','\uC218','\uBAA9','\uAE08','\uD1A0'],
    dayNamesMin: ['\uC77C','\uC6D4','\uD654','\uC218','\uBAA9','\uAE08','\uD1A0'],
    weekHeader: 'Wk',
    dateFormat: 'yy-mm-dd',
    firstDay: 0,
    isRTL: false,
    showMonthAfterYear: true,
    yearSuffix: '\uB144'};
datepicker.setDefaults(datepicker.regional['ko']);

var i18nDatepickerKo = datepicker.regional['ko'];


/* Kyrgyz (UTF-8) initialisation for the jQuery UI date picker plugin. */
/* Written by Sergey Kartashov (ebishkek@yandex.ru). */


datepicker.regional['ky'] = {
    closeText: '\u0416\u0430\u0431\u0443\u0443',
    prevText: '&#x3c;\u041C\u0443\u0440',
    nextText: '\u041A\u0438\u0439&#x3e;',
    currentText: '\u0411\u04AF\u0433\u04AF\u043D',
    monthNames: ['\u042F\u043D\u0432\u0430\u0440\u044C','\u0424\u0435\u0432\u0440\u0430\u043B\u044C','\u041C\u0430\u0440\u0442','\u0410\u043F\u0440\u0435\u043B\u044C','\u041C\u0430\u0439','\u0418\u044E\u043D\u044C',
    '\u0418\u044E\u043B\u044C','\u0410\u0432\u0433\u0443\u0441\u0442','\u0421\u0435\u043D\u0442\u044F\u0431\u0440\u044C','\u041E\u043A\u0442\u044F\u0431\u0440\u044C','\u041D\u043E\u044F\u0431\u0440\u044C','\u0414\u0435\u043A\u0430\u0431\u0440\u044C'],
    monthNamesShort: ['\u042F\u043D\u0432','\u0424\u0435\u0432','\u041C\u0430\u0440','\u0410\u043F\u0440','\u041C\u0430\u0439','\u0418\u044E\u043D',
    '\u0418\u044E\u043B','\u0410\u0432\u0433','\u0421\u0435\u043D','\u041E\u043A\u0442','\u041D\u043E\u044F','\u0414\u0435\u043A'],
    dayNames: ['\u0436\u0435\u043A\u0448\u0435\u043C\u0431\u0438', '\u0434\u04AF\u0439\u0448\u04E9\u043C\u0431\u04AF', '\u0448\u0435\u0439\u0448\u0435\u043C\u0431\u0438', '\u0448\u0430\u0440\u0448\u0435\u043C\u0431\u0438', '\u0431\u0435\u0439\u0448\u0435\u043C\u0431\u0438', '\u0436\u0443\u043C\u0430', '\u0438\u0448\u0435\u043C\u0431\u0438'],
    dayNamesShort: ['\u0436\u0435\u043A', '\u0434\u04AF\u0439', '\u0448\u0435\u0439', '\u0448\u0430\u0440', '\u0431\u0435\u0439', '\u0436\u0443\u043C', '\u0438\u0448\u0435'],
    dayNamesMin: ['\u0416\u043A','\u0414\u0448','\u0428\u0448','\u0428\u0440','\u0411\u0448','\u0416\u043C','\u0418\u0448'],
    weekHeader: '\u0416\u0443\u043C',
    dateFormat: 'dd.mm.yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''
};
datepicker.setDefaults(datepicker.regional['ky']);

var i18nDatepickerKy = datepicker.regional['ky'];


/* Luxembourgish initialisation for the jQuery UI date picker plugin. */
/* Written by Michel Weimerskirch <michel@weimerskirch.net> */


datepicker.regional['lb'] = {
    closeText: 'F\u00E4erdeg',
    prevText: 'Zr\u00E9ck',
    nextText: 'Weider',
    currentText: 'Haut',
    monthNames: ['Januar','Februar','M\u00E4erz','Abr\u00EBll','Mee','Juni',
    'Juli','August','September','Oktober','November','Dezember'],
    monthNamesShort: ['Jan', 'Feb', 'M\u00E4e', 'Abr', 'Mee', 'Jun',
    'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
    dayNames: ['Sonndeg', 'M\u00E9indeg', 'D\u00EBnschdeg', 'M\u00EBttwoch', 'Donneschdeg', 'Freideg', 'Samschdeg'],
    dayNamesShort: ['Son', 'M\u00E9i', 'D\u00EBn', 'M\u00EBt', 'Don', 'Fre', 'Sam'],
    dayNamesMin: ['So','M\u00E9','D\u00EB','M\u00EB','Do','Fr','Sa'],
    weekHeader: 'W',
    dateFormat: 'dd.mm.yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['lb']);

var i18nDatepickerLb = datepicker.regional['lb'];


/* Lithuanian (UTF-8) initialisation for the jQuery UI date picker plugin. */
/* @author Arturas Paleicikas <arturas@avalon.lt> */


datepicker.regional['lt'] = {
    closeText: 'U\u017Edaryti',
    prevText: '&#x3C;Atgal',
    nextText: 'Pirmyn&#x3E;',
    currentText: '\u0160iandien',
    monthNames: ['Sausis','Vasaris','Kovas','Balandis','Gegu\u017E\u0117','Bir\u017Eelis',
    'Liepa','Rugpj\u016Btis','Rugs\u0117jis','Spalis','Lapkritis','Gruodis'],
    monthNamesShort: ['Sau','Vas','Kov','Bal','Geg','Bir',
    'Lie','Rugp','Rugs','Spa','Lap','Gru'],
    dayNames: ['sekmadienis','pirmadienis','antradienis','tre\u010Diadienis','ketvirtadienis','penktadienis','\u0161e\u0161tadienis'],
    dayNamesShort: ['sek','pir','ant','tre','ket','pen','\u0161e\u0161'],
    dayNamesMin: ['Se','Pr','An','Tr','Ke','Pe','\u0160e'],
    weekHeader: 'SAV',
    dateFormat: 'yy-mm-dd',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: true,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['lt']);

var i18nDatepickerLt = datepicker.regional['lt'];


/* Latvian (UTF-8) initialisation for the jQuery UI date picker plugin. */
/* @author Arturas Paleicikas <arturas.paleicikas@metasite.net> */


datepicker.regional['lv'] = {
    closeText: 'Aizv\u0113rt',
    prevText: 'Iepr.',
    nextText: 'N\u0101k.',
    currentText: '\u0160odien',
    monthNames: ['Janv\u0101ris','Febru\u0101ris','Marts','Apr\u012Blis','Maijs','J\u016Bnijs',
    'J\u016Blijs','Augusts','Septembris','Oktobris','Novembris','Decembris'],
    monthNamesShort: ['Jan','Feb','Mar','Apr','Mai','J\u016Bn',
    'J\u016Bl','Aug','Sep','Okt','Nov','Dec'],
    dayNames: ['sv\u0113tdiena','pirmdiena','otrdiena','tre\u0161diena','ceturtdiena','piektdiena','sestdiena'],
    dayNamesShort: ['svt','prm','otr','tre','ctr','pkt','sst'],
    dayNamesMin: ['Sv','Pr','Ot','Tr','Ct','Pk','Ss'],
    weekHeader: 'Ned.',
    dateFormat: 'dd.mm.yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['lv']);

var i18nDatepickerLv = datepicker.regional['lv'];


/* Macedonian i18n for the jQuery UI date picker plugin. */
/* Written by Stojce Slavkovski. */


datepicker.regional['mk'] = {
    closeText: '\u0417\u0430\u0442\u0432\u043E\u0440\u0438',
    prevText: '&#x3C;',
    nextText: '&#x3E;',
    currentText: '\u0414\u0435\u043D\u0435\u0441',
    monthNames: ['\u0408\u0430\u043D\u0443\u0430\u0440\u0438','\u0424\u0435\u0432\u0440\u0443\u0430\u0440\u0438','\u041C\u0430\u0440\u0442','\u0410\u043F\u0440\u0438\u043B','\u041C\u0430\u0458','\u0408\u0443\u043D\u0438',
    '\u0408\u0443\u043B\u0438','\u0410\u0432\u0433\u0443\u0441\u0442','\u0421\u0435\u043F\u0442\u0435\u043C\u0432\u0440\u0438','\u041E\u043A\u0442\u043E\u043C\u0432\u0440\u0438','\u041D\u043E\u0435\u043C\u0432\u0440\u0438','\u0414\u0435\u043A\u0435\u043C\u0432\u0440\u0438'],
    monthNamesShort: ['\u0408\u0430\u043D','\u0424\u0435\u0432','\u041C\u0430\u0440','\u0410\u043F\u0440','\u041C\u0430\u0458','\u0408\u0443\u043D',
    '\u0408\u0443\u043B','\u0410\u0432\u0433','\u0421\u0435\u043F','\u041E\u043A\u0442','\u041D\u043E\u0435','\u0414\u0435\u043A'],
    dayNames: ['\u041D\u0435\u0434\u0435\u043B\u0430','\u041F\u043E\u043D\u0435\u0434\u0435\u043B\u043D\u0438\u043A','\u0412\u0442\u043E\u0440\u043D\u0438\u043A','\u0421\u0440\u0435\u0434\u0430','\u0427\u0435\u0442\u0432\u0440\u0442\u043E\u043A','\u041F\u0435\u0442\u043E\u043A','\u0421\u0430\u0431\u043E\u0442\u0430'],
    dayNamesShort: ['\u041D\u0435\u0434','\u041F\u043E\u043D','\u0412\u0442\u043E','\u0421\u0440\u0435','\u0427\u0435\u0442','\u041F\u0435\u0442','\u0421\u0430\u0431'],
    dayNamesMin: ['\u041D\u0435','\u041F\u043E','\u0412\u0442','\u0421\u0440','\u0427\u0435','\u041F\u0435','\u0421\u0430'],
    weekHeader: '\u0421\u0435\u0434',
    dateFormat: 'dd.mm.yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['mk']);

var i18nDatepickerMk = datepicker.regional['mk'];


/* Malayalam (UTF-8) initialisation for the jQuery UI date picker plugin. */
/* Written by Saji Nediyanchath (saji89@gmail.com). */


datepicker.regional['ml'] = {
    closeText: '\u0D36\u0D30\u0D3F',
    prevText: '\u0D2E\u0D41\u0D28\u0D4D\u0D28\u0D24\u0D4D\u0D24\u0D46',
    nextText: '\u0D05\u0D1F\u0D41\u0D24\u0D4D\u0D24\u0D24\u0D4D ',
    currentText: '\u0D07\u0D28\u0D4D\u0D28\u0D4D',
    monthNames: ['\u0D1C\u0D28\u0D41\u0D35\u0D30\u0D3F','\u0D2B\u0D46\u0D2C\u0D4D\u0D30\u0D41\u0D35\u0D30\u0D3F','\u0D2E\u0D3E\u0D30\u0D4D\u200D\u0D1A\u0D4D\u0D1A\u0D4D','\u0D0F\u0D2A\u0D4D\u0D30\u0D3F\u0D32\u0D4D\u200D','\u0D2E\u0D47\u0D2F\u0D4D','\u0D1C\u0D42\u0D23\u0D4D\u200D',
    '\u0D1C\u0D42\u0D32\u0D48','\u0D06\u0D17\u0D38\u0D4D\u0D31\u0D4D\u0D31\u0D4D','\u0D38\u0D46\u0D2A\u0D4D\u0D31\u0D4D\u0D31\u0D02\u0D2C\u0D30\u0D4D\u200D','\u0D12\u0D15\u0D4D\u0D1F\u0D4B\u0D2C\u0D30\u0D4D\u200D','\u0D28\u0D35\u0D02\u0D2C\u0D30\u0D4D\u200D','\u0D21\u0D3F\u0D38\u0D02\u0D2C\u0D30\u0D4D\u200D'],
    monthNamesShort: ['\u0D1C\u0D28\u0D41', '\u0D2B\u0D46\u0D2C\u0D4D', '\u0D2E\u0D3E\u0D30\u0D4D\u200D', '\u0D0F\u0D2A\u0D4D\u0D30\u0D3F', '\u0D2E\u0D47\u0D2F\u0D4D', '\u0D1C\u0D42\u0D23\u0D4D\u200D',
    '\u0D1C\u0D42\u0D32\u0D3E', '\u0D06\u0D17', '\u0D38\u0D46\u0D2A\u0D4D', '\u0D12\u0D15\u0D4D\u0D1F\u0D4B', '\u0D28\u0D35\u0D02', '\u0D21\u0D3F\u0D38'],
    dayNames: ['\u0D1E\u0D3E\u0D2F\u0D30\u0D4D\u200D', '\u0D24\u0D3F\u0D19\u0D4D\u0D15\u0D33\u0D4D\u200D', '\u0D1A\u0D4A\u0D35\u0D4D\u0D35', '\u0D2C\u0D41\u0D27\u0D28\u0D4D\u200D', '\u0D35\u0D4D\u0D2F\u0D3E\u0D34\u0D02', '\u0D35\u0D46\u0D33\u0D4D\u0D33\u0D3F', '\u0D36\u0D28\u0D3F'],
    dayNamesShort: ['\u0D1E\u0D3E\u0D2F', '\u0D24\u0D3F\u0D19\u0D4D\u0D15', '\u0D1A\u0D4A\u0D35\u0D4D\u0D35', '\u0D2C\u0D41\u0D27', '\u0D35\u0D4D\u0D2F\u0D3E\u0D34\u0D02', '\u0D35\u0D46\u0D33\u0D4D\u0D33\u0D3F', '\u0D36\u0D28\u0D3F'],
    dayNamesMin: ['\u0D1E\u0D3E','\u0D24\u0D3F','\u0D1A\u0D4A','\u0D2C\u0D41','\u0D35\u0D4D\u0D2F\u0D3E','\u0D35\u0D46','\u0D36'],
    weekHeader: '\u0D06',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['ml']);

var i18nDatepickerMl = datepicker.regional['ml'];


/* Malaysian initialisation for the jQuery UI date picker plugin. */
/* Written by Mohd Nawawi Mohamad Jamili (nawawi@ronggeng.net). */


datepicker.regional['ms'] = {
    closeText: 'Tutup',
    prevText: '&#x3C;Sebelum',
    nextText: 'Selepas&#x3E;',
    currentText: 'hari ini',
    monthNames: ['Januari','Februari','Mac','April','Mei','Jun',
    'Julai','Ogos','September','Oktober','November','Disember'],
    monthNamesShort: ['Jan','Feb','Mac','Apr','Mei','Jun',
    'Jul','Ogo','Sep','Okt','Nov','Dis'],
    dayNames: ['Ahad','Isnin','Selasa','Rabu','Khamis','Jumaat','Sabtu'],
    dayNamesShort: ['Aha','Isn','Sel','Rab','kha','Jum','Sab'],
    dayNamesMin: ['Ah','Is','Se','Ra','Kh','Ju','Sa'],
    weekHeader: 'Mg',
    dateFormat: 'dd/mm/yy',
    firstDay: 0,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['ms']);

var i18nDatepickerMs = datepicker.regional['ms'];


/* Norwegian Bokm\u00E5l initialisation for the jQuery UI date picker plugin. */
/* Written by Bj\u00F8rn Johansen (post@bjornjohansen.no). */


datepicker.regional['nb'] = {
    closeText: 'Lukk',
    prevText: '&#xAB;Forrige',
    nextText: 'Neste&#xBB;',
    currentText: 'I dag',
    monthNames: ['januar','februar','mars','april','mai','juni','juli','august','september','oktober','november','desember'],
    monthNamesShort: ['jan','feb','mar','apr','mai','jun','jul','aug','sep','okt','nov','des'],
    dayNamesShort: ['s\u00F8n','man','tir','ons','tor','fre','l\u00F8r'],
    dayNames: ['s\u00F8ndag','mandag','tirsdag','onsdag','torsdag','fredag','l\u00F8rdag'],
    dayNamesMin: ['s\u00F8','ma','ti','on','to','fr','l\u00F8'],
    weekHeader: 'Uke',
    dateFormat: 'dd.mm.yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''
};
datepicker.setDefaults(datepicker.regional['nb']);

var i18nDatepickerNb = datepicker.regional['nb'];


/* Dutch (Belgium) initialisation for the jQuery UI date picker plugin. */
/* David De Sloovere @DavidDeSloovere */


datepicker.regional['nl-BE'] = {
    closeText: 'Sluiten',
    prevText: '\u2190',
    nextText: '\u2192',
    currentText: 'Vandaag',
    monthNames: ['januari', 'februari', 'maart', 'april', 'mei', 'juni',
    'juli', 'augustus', 'september', 'oktober', 'november', 'december'],
    monthNamesShort: ['jan', 'feb', 'mrt', 'apr', 'mei', 'jun',
    'jul', 'aug', 'sep', 'okt', 'nov', 'dec'],
    dayNames: ['zondag', 'maandag', 'dinsdag', 'woensdag', 'donderdag', 'vrijdag', 'zaterdag'],
    dayNamesShort: ['zon', 'maa', 'din', 'woe', 'don', 'vri', 'zat'],
    dayNamesMin: ['zo', 'ma', 'di', 'wo', 'do', 'vr', 'za'],
    weekHeader: 'Wk',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['nl-BE']);

var i18nDatepickerNlBe = datepicker.regional['nl-BE'];


/* Dutch (UTF-8) initialisation for the jQuery UI date picker plugin. */
/* Written by Mathias Bynens <http://mathiasbynens.be/> */


datepicker.regional.nl = {
    closeText: 'Sluiten',
    prevText: '\u2190',
    nextText: '\u2192',
    currentText: 'Vandaag',
    monthNames: ['januari', 'februari', 'maart', 'april', 'mei', 'juni',
    'juli', 'augustus', 'september', 'oktober', 'november', 'december'],
    monthNamesShort: ['jan', 'feb', 'mrt', 'apr', 'mei', 'jun',
    'jul', 'aug', 'sep', 'okt', 'nov', 'dec'],
    dayNames: ['zondag', 'maandag', 'dinsdag', 'woensdag', 'donderdag', 'vrijdag', 'zaterdag'],
    dayNamesShort: ['zon', 'maa', 'din', 'woe', 'don', 'vri', 'zat'],
    dayNamesMin: ['zo', 'ma', 'di', 'wo', 'do', 'vr', 'za'],
    weekHeader: 'Wk',
    dateFormat: 'dd-mm-yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional.nl);

var i18nDatepickerNl = datepicker.regional.nl;


/* Norwegian Nynorsk initialisation for the jQuery UI date picker plugin. */
/* Written by Bj\u00F8rn Johansen (post@bjornjohansen.no). */


datepicker.regional['nn'] = {
    closeText: 'Lukk',
    prevText: '&#xAB;F\u00F8rre',
    nextText: 'Neste&#xBB;',
    currentText: 'I dag',
    monthNames: ['januar','februar','mars','april','mai','juni','juli','august','september','oktober','november','desember'],
    monthNamesShort: ['jan','feb','mar','apr','mai','jun','jul','aug','sep','okt','nov','des'],
    dayNamesShort: ['sun','m\u00E5n','tys','ons','tor','fre','lau'],
    dayNames: ['sundag','m\u00E5ndag','tysdag','onsdag','torsdag','fredag','laurdag'],
    dayNamesMin: ['su','m\u00E5','ty','on','to','fr','la'],
    weekHeader: 'Veke',
    dateFormat: 'dd.mm.yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''
};
datepicker.setDefaults(datepicker.regional['nn']);

var i18nDatepickerNn = datepicker.regional['nn'];


/* Norwegian initialisation for the jQuery UI date picker plugin. */
/* Written by Naimdjon Takhirov (naimdjon@gmail.com). */



datepicker.regional['no'] = {
    closeText: 'Lukk',
    prevText: '&#xAB;Forrige',
    nextText: 'Neste&#xBB;',
    currentText: 'I dag',
    monthNames: ['januar','februar','mars','april','mai','juni','juli','august','september','oktober','november','desember'],
    monthNamesShort: ['jan','feb','mar','apr','mai','jun','jul','aug','sep','okt','nov','des'],
    dayNamesShort: ['s\u00F8n','man','tir','ons','tor','fre','l\u00F8r'],
    dayNames: ['s\u00F8ndag','mandag','tirsdag','onsdag','torsdag','fredag','l\u00F8rdag'],
    dayNamesMin: ['s\u00F8','ma','ti','on','to','fr','l\u00F8'],
    weekHeader: 'Uke',
    dateFormat: 'dd.mm.yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''
};
datepicker.setDefaults(datepicker.regional['no']);

var i18nDatepickerNo = datepicker.regional['no'];


/* Polish initialisation for the jQuery UI date picker plugin. */
/* Written by Jacek Wysocki (jacek.wysocki@gmail.com). */


datepicker.regional['pl'] = {
    closeText: 'Zamknij',
    prevText: '&#x3C;Poprzedni',
    nextText: 'Nast\u0119pny&#x3E;',
    currentText: 'Dzi\u015B',
    monthNames: ['Stycze\u0144','Luty','Marzec','Kwiecie\u0144','Maj','Czerwiec',
    'Lipiec','Sierpie\u0144','Wrzesie\u0144','Pa\u017Adziernik','Listopad','Grudzie\u0144'],
    monthNamesShort: ['Sty','Lu','Mar','Kw','Maj','Cze',
    'Lip','Sie','Wrz','Pa','Lis','Gru'],
    dayNames: ['Niedziela','Poniedzia\u0142ek','Wtorek','\u015Aroda','Czwartek','Pi\u0105tek','Sobota'],
    dayNamesShort: ['Nie','Pn','Wt','\u015Ar','Czw','Pt','So'],
    dayNamesMin: ['N','Pn','Wt','\u015Ar','Cz','Pt','So'],
    weekHeader: 'Tydz',
    dateFormat: 'dd.mm.yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['pl']);

var i18nDatepickerPl = datepicker.regional['pl'];


/* Brazilian initialisation for the jQuery UI date picker plugin. */
/* Written by Leonildo Costa Silva (leocsilva@gmail.com). */


datepicker.regional['pt-BR'] = {
    closeText: 'Fechar',
    prevText: '&#x3C;Anterior',
    nextText: 'Pr\u00F3ximo&#x3E;',
    currentText: 'Hoje',
    monthNames: ['Janeiro','Fevereiro','Mar\u00E7o','Abril','Maio','Junho',
    'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
    'Jul','Ago','Set','Out','Nov','Dez'],
    dayNames: ['Domingo','Segunda-feira','Ter\u00E7a-feira','Quarta-feira','Quinta-feira','Sexta-feira','S\u00E1bado'],
    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','S\u00E1b'],
    dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','S\u00E1b'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 0,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['pt-BR']);

var i18nDatepickerPtBr = datepicker.regional['pt-BR'];


/* Portuguese initialisation for the jQuery UI date picker plugin. */


datepicker.regional['pt'] = {
    closeText: 'Fechar',
    prevText: 'Anterior',
    nextText: 'Seguinte',
    currentText: 'Hoje',
    monthNames: ['Janeiro','Fevereiro','Mar\u00E7o','Abril','Maio','Junho',
    'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
    'Jul','Ago','Set','Out','Nov','Dez'],
    dayNames: ['Domingo','Segunda-feira','Ter\u00E7a-feira','Quarta-feira','Quinta-feira','Sexta-feira','S\u00E1bado'],
    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','S\u00E1b'],
    dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','S\u00E1b'],
    weekHeader: 'Sem',
    dateFormat: 'dd/mm/yy',
    firstDay: 0,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['pt']);

var i18nDatepickerPt = datepicker.regional['pt'];


/* Romansh initialisation for the jQuery UI date picker plugin. */
/* Written by Yvonne Gienal (yvonne.gienal@educa.ch). */


datepicker.regional['rm'] = {
    closeText: 'Serrar',
    prevText: '&#x3C;Suandant',
    nextText: 'Precedent&#x3E;',
    currentText: 'Actual',
    monthNames: ['Schaner','Favrer','Mars','Avrigl','Matg','Zercladur', 'Fanadur','Avust','Settember','October','November','December'],
    monthNamesShort: ['Scha','Fev','Mar','Avr','Matg','Zer', 'Fan','Avu','Sett','Oct','Nov','Dec'],
    dayNames: ['Dumengia','Glindesdi','Mardi','Mesemna','Gievgia','Venderdi','Sonda'],
    dayNamesShort: ['Dum','Gli','Mar','Mes','Gie','Ven','Som'],
    dayNamesMin: ['Du','Gl','Ma','Me','Gi','Ve','So'],
    weekHeader: 'emna',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['rm']);

var i18nDatepickerRm = datepicker.regional['rm'];


/* Romanian initialisation for the jQuery UI date picker plugin.
 *
 * Written by Edmond L. (ll_edmond@walla.com)
 * and Ionut G. Stan (ionut.g.stan@gmail.com)
 */


datepicker.regional['ro'] = {
    closeText: '\u00CEnchide',
    prevText: '&#xAB; Luna precedent\u0103',
    nextText: 'Luna urm\u0103toare &#xBB;',
    currentText: 'Azi',
    monthNames: ['Ianuarie','Februarie','Martie','Aprilie','Mai','Iunie',
    'Iulie','August','Septembrie','Octombrie','Noiembrie','Decembrie'],
    monthNamesShort: ['Ian', 'Feb', 'Mar', 'Apr', 'Mai', 'Iun',
    'Iul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
    dayNames: ['Duminic\u0103', 'Luni', 'Mar\u0163i', 'Miercuri', 'Joi', 'Vineri', 'S\u00E2mb\u0103t\u0103'],
    dayNamesShort: ['Dum', 'Lun', 'Mar', 'Mie', 'Joi', 'Vin', 'S\u00E2m'],
    dayNamesMin: ['Du','Lu','Ma','Mi','Jo','Vi','S\u00E2'],
    weekHeader: 'S\u0103pt',
    dateFormat: 'dd.mm.yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['ro']);

var i18nDatepickerRo = datepicker.regional['ro'];


/* Russian (UTF-8) initialisation for the jQuery UI date picker plugin. */
/* Written by Andrew Stromnov (stromnov@gmail.com). */


datepicker.regional['ru'] = {
    closeText: '\u0417\u0430\u043A\u0440\u044B\u0442\u044C',
    prevText: '&#x3C;\u041F\u0440\u0435\u0434',
    nextText: '\u0421\u043B\u0435\u0434&#x3E;',
    currentText: '\u0421\u0435\u0433\u043E\u0434\u043D\u044F',
    monthNames: ['\u042F\u043D\u0432\u0430\u0440\u044C','\u0424\u0435\u0432\u0440\u0430\u043B\u044C','\u041C\u0430\u0440\u0442','\u0410\u043F\u0440\u0435\u043B\u044C','\u041C\u0430\u0439','\u0418\u044E\u043D\u044C',
    '\u0418\u044E\u043B\u044C','\u0410\u0432\u0433\u0443\u0441\u0442','\u0421\u0435\u043D\u0442\u044F\u0431\u0440\u044C','\u041E\u043A\u0442\u044F\u0431\u0440\u044C','\u041D\u043E\u044F\u0431\u0440\u044C','\u0414\u0435\u043A\u0430\u0431\u0440\u044C'],
    monthNamesShort: ['\u042F\u043D\u0432','\u0424\u0435\u0432','\u041C\u0430\u0440','\u0410\u043F\u0440','\u041C\u0430\u0439','\u0418\u044E\u043D',
    '\u0418\u044E\u043B','\u0410\u0432\u0433','\u0421\u0435\u043D','\u041E\u043A\u0442','\u041D\u043E\u044F','\u0414\u0435\u043A'],
    dayNames: ['\u0432\u043E\u0441\u043A\u0440\u0435\u0441\u0435\u043D\u044C\u0435','\u043F\u043E\u043D\u0435\u0434\u0435\u043B\u044C\u043D\u0438\u043A','\u0432\u0442\u043E\u0440\u043D\u0438\u043A','\u0441\u0440\u0435\u0434\u0430','\u0447\u0435\u0442\u0432\u0435\u0440\u0433','\u043F\u044F\u0442\u043D\u0438\u0446\u0430','\u0441\u0443\u0431\u0431\u043E\u0442\u0430'],
    dayNamesShort: ['\u0432\u0441\u043A','\u043F\u043D\u0434','\u0432\u0442\u0440','\u0441\u0440\u0434','\u0447\u0442\u0432','\u043F\u0442\u043D','\u0441\u0431\u0442'],
    dayNamesMin: ['\u0412\u0441','\u041F\u043D','\u0412\u0442','\u0421\u0440','\u0427\u0442','\u041F\u0442','\u0421\u0431'],
    weekHeader: '\u041D\u0435\u0434',
    dateFormat: 'dd.mm.yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['ru']);

var i18nDatepickerRu = datepicker.regional['ru'];


/* Slovak initialisation for the jQuery UI date picker plugin. */
/* Written by Vojtech Rinik (vojto@hmm.sk). */


datepicker.regional['sk'] = {
    closeText: 'Zavrie\u0165',
    prevText: '&#x3C;Predch\u00E1dzaj\u00FAci',
    nextText: 'Nasleduj\u00FAci&#x3E;',
    currentText: 'Dnes',
    monthNames: ['janu\u00E1r','febru\u00E1r','marec','apr\u00EDl','m\u00E1j','j\u00FAn',
    'j\u00FAl','august','september','okt\u00F3ber','november','december'],
    monthNamesShort: ['Jan','Feb','Mar','Apr','M\u00E1j','J\u00FAn',
    'J\u00FAl','Aug','Sep','Okt','Nov','Dec'],
    dayNames: ['nede\u013Ea','pondelok','utorok','streda','\u0161tvrtok','piatok','sobota'],
    dayNamesShort: ['Ned','Pon','Uto','Str','\u0160tv','Pia','Sob'],
    dayNamesMin: ['Ne','Po','Ut','St','\u0160t','Pia','So'],
    weekHeader: 'Ty',
    dateFormat: 'dd.mm.yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['sk']);

var i18nDatepickerSk = datepicker.regional['sk'];


/* Slovenian initialisation for the jQuery UI date picker plugin. */
/* Written by Jaka Jancar (jaka@kubje.org). */
/* c = \u010D, s = \u0161 z = \u017E C = \u010C S = \u0160 Z = \u017D */


datepicker.regional['sl'] = {
    closeText: 'Zapri',
    prevText: '&#x3C;Prej\u0161nji',
    nextText: 'Naslednji&#x3E;',
    currentText: 'Trenutni',
    monthNames: ['Januar','Februar','Marec','April','Maj','Junij',
    'Julij','Avgust','September','Oktober','November','December'],
    monthNamesShort: ['Jan','Feb','Mar','Apr','Maj','Jun',
    'Jul','Avg','Sep','Okt','Nov','Dec'],
    dayNames: ['Nedelja','Ponedeljek','Torek','Sreda','\u010Cetrtek','Petek','Sobota'],
    dayNamesShort: ['Ned','Pon','Tor','Sre','\u010Cet','Pet','Sob'],
    dayNamesMin: ['Ne','Po','To','Sr','\u010Ce','Pe','So'],
    weekHeader: 'Teden',
    dateFormat: 'dd.mm.yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['sl']);

var i18nDatepickerSl = datepicker.regional['sl'];


/* Albanian initialisation for the jQuery UI date picker plugin. */
/* Written by Flakron Bytyqi (flakron@gmail.com). */


datepicker.regional['sq'] = {
    closeText: 'mbylle',
    prevText: '&#x3C;mbrapa',
    nextText: 'P\u00EBrpara&#x3E;',
    currentText: 'sot',
    monthNames: ['Janar','Shkurt','Mars','Prill','Maj','Qershor',
    'Korrik','Gusht','Shtator','Tetor','N\u00EBntor','Dhjetor'],
    monthNamesShort: ['Jan','Shk','Mar','Pri','Maj','Qer',
    'Kor','Gus','Sht','Tet','N\u00EBn','Dhj'],
    dayNames: ['E Diel','E H\u00EBn\u00EB','E Mart\u00EB','E M\u00EBrkur\u00EB','E Enjte','E Premte','E Shtune'],
    dayNamesShort: ['Di','H\u00EB','Ma','M\u00EB','En','Pr','Sh'],
    dayNamesMin: ['Di','H\u00EB','Ma','M\u00EB','En','Pr','Sh'],
    weekHeader: 'Ja',
    dateFormat: 'dd.mm.yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['sq']);

var i18nDatepickerSq = datepicker.regional['sq'];


/* Serbian i18n for the jQuery UI date picker plugin. */
/* Written by Dejan Dimi\u0107. */


datepicker.regional['sr-SR'] = {
    closeText: 'Zatvori',
    prevText: '&#x3C;',
    nextText: '&#x3E;',
    currentText: 'Danas',
    monthNames: ['Januar','Februar','Mart','April','Maj','Jun',
    'Jul','Avgust','Septembar','Oktobar','Novembar','Decembar'],
    monthNamesShort: ['Jan','Feb','Mar','Apr','Maj','Jun',
    'Jul','Avg','Sep','Okt','Nov','Dec'],
    dayNames: ['Nedelja','Ponedeljak','Utorak','Sreda','\u010Cetvrtak','Petak','Subota'],
    dayNamesShort: ['Ned','Pon','Uto','Sre','\u010Cet','Pet','Sub'],
    dayNamesMin: ['Ne','Po','Ut','Sr','\u010Ce','Pe','Su'],
    weekHeader: 'Sed',
    dateFormat: 'dd.mm.yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['sr-SR']);

var i18nDatepickerSrSr = datepicker.regional['sr-SR'];


/* Serbian i18n for the jQuery UI date picker plugin. */
/* Written by Dejan Dimi\u0107. */


datepicker.regional['sr'] = {
    closeText: '\u0417\u0430\u0442\u0432\u043E\u0440\u0438',
    prevText: '&#x3C;',
    nextText: '&#x3E;',
    currentText: '\u0414\u0430\u043D\u0430\u0441',
    monthNames: ['\u0408\u0430\u043D\u0443\u0430\u0440','\u0424\u0435\u0431\u0440\u0443\u0430\u0440','\u041C\u0430\u0440\u0442','\u0410\u043F\u0440\u0438\u043B','\u041C\u0430\u0458','\u0408\u0443\u043D',
    '\u0408\u0443\u043B','\u0410\u0432\u0433\u0443\u0441\u0442','\u0421\u0435\u043F\u0442\u0435\u043C\u0431\u0430\u0440','\u041E\u043A\u0442\u043E\u0431\u0430\u0440','\u041D\u043E\u0432\u0435\u043C\u0431\u0430\u0440','\u0414\u0435\u0446\u0435\u043C\u0431\u0430\u0440'],
    monthNamesShort: ['\u0408\u0430\u043D','\u0424\u0435\u0431','\u041C\u0430\u0440','\u0410\u043F\u0440','\u041C\u0430\u0458','\u0408\u0443\u043D',
    '\u0408\u0443\u043B','\u0410\u0432\u0433','\u0421\u0435\u043F','\u041E\u043A\u0442','\u041D\u043E\u0432','\u0414\u0435\u0446'],
    dayNames: ['\u041D\u0435\u0434\u0435\u0459\u0430','\u041F\u043E\u043D\u0435\u0434\u0435\u0459\u0430\u043A','\u0423\u0442\u043E\u0440\u0430\u043A','\u0421\u0440\u0435\u0434\u0430','\u0427\u0435\u0442\u0432\u0440\u0442\u0430\u043A','\u041F\u0435\u0442\u0430\u043A','\u0421\u0443\u0431\u043E\u0442\u0430'],
    dayNamesShort: ['\u041D\u0435\u0434','\u041F\u043E\u043D','\u0423\u0442\u043E','\u0421\u0440\u0435','\u0427\u0435\u0442','\u041F\u0435\u0442','\u0421\u0443\u0431'],
    dayNamesMin: ['\u041D\u0435','\u041F\u043E','\u0423\u0442','\u0421\u0440','\u0427\u0435','\u041F\u0435','\u0421\u0443'],
    weekHeader: '\u0421\u0435\u0434',
    dateFormat: 'dd.mm.yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['sr']);

var i18nDatepickerSr = datepicker.regional['sr'];


/* Swedish initialisation for the jQuery UI date picker plugin. */
/* Written by Anders Ekdahl ( anders@nomadiz.se). */


datepicker.regional['sv'] = {
    closeText: 'St\u00E4ng',
    prevText: '&#xAB;F\u00F6rra',
    nextText: 'N\u00E4sta&#xBB;',
    currentText: 'Idag',
    monthNames: ['Januari','Februari','Mars','April','Maj','Juni',
    'Juli','Augusti','September','Oktober','November','December'],
    monthNamesShort: ['Jan','Feb','Mar','Apr','Maj','Jun',
    'Jul','Aug','Sep','Okt','Nov','Dec'],
    dayNamesShort: ['S\u00F6n','M\u00E5n','Tis','Ons','Tor','Fre','L\u00F6r'],
    dayNames: ['S\u00F6ndag','M\u00E5ndag','Tisdag','Onsdag','Torsdag','Fredag','L\u00F6rdag'],
    dayNamesMin: ['S\u00F6','M\u00E5','Ti','On','To','Fr','L\u00F6'],
    weekHeader: 'Ve',
    dateFormat: 'yy-mm-dd',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['sv']);

var i18nDatepickerSv = datepicker.regional['sv'];


/* Tamil (UTF-8) initialisation for the jQuery UI date picker plugin. */
/* Written by S A Sureshkumar (saskumar@live.com). */


datepicker.regional['ta'] = {
    closeText: '\u0BAE\u0BC2\u0B9F\u0BC1',
    prevText: '\u0BAE\u0BC1\u0BA9\u0BCD\u0BA9\u0BC8\u0BAF\u0BA4\u0BC1',
    nextText: '\u0B85\u0B9F\u0BC1\u0BA4\u0BCD\u0BA4\u0BA4\u0BC1',
    currentText: '\u0B87\u0BA9\u0BCD\u0BB1\u0BC1',
    monthNames: ['\u0BA4\u0BC8','\u0BAE\u0BBE\u0B9A\u0BBF','\u0BAA\u0B99\u0BCD\u0B95\u0BC1\u0BA9\u0BBF','\u0B9A\u0BBF\u0BA4\u0BCD\u0BA4\u0BBF\u0BB0\u0BC8','\u0BB5\u0BC8\u0B95\u0BBE\u0B9A\u0BBF','\u0B86\u0BA9\u0BBF',
    '\u0B86\u0B9F\u0BBF','\u0B86\u0BB5\u0BA3\u0BBF','\u0BAA\u0BC1\u0BB0\u0B9F\u0BCD\u0B9F\u0BBE\u0B9A\u0BBF','\u0B90\u0BAA\u0BCD\u0BAA\u0B9A\u0BBF','\u0B95\u0BBE\u0BB0\u0BCD\u0BA4\u0BCD\u0BA4\u0BBF\u0B95\u0BC8','\u0BAE\u0BBE\u0BB0\u0BCD\u0B95\u0BB4\u0BBF'],
    monthNamesShort: ['\u0BA4\u0BC8','\u0BAE\u0BBE\u0B9A\u0BBF','\u0BAA\u0B99\u0BCD','\u0B9A\u0BBF\u0BA4\u0BCD','\u0BB5\u0BC8\u0B95\u0BBE','\u0B86\u0BA9\u0BBF',
    '\u0B86\u0B9F\u0BBF','\u0B86\u0BB5','\u0BAA\u0BC1\u0BB0','\u0B90\u0BAA\u0BCD','\u0B95\u0BBE\u0BB0\u0BCD','\u0BAE\u0BBE\u0BB0\u0BCD'],
    dayNames: ['\u0B9E\u0BBE\u0BAF\u0BBF\u0BB1\u0BCD\u0BB1\u0BC1\u0B95\u0BCD\u0B95\u0BBF\u0BB4\u0BAE\u0BC8','\u0BA4\u0BBF\u0B99\u0BCD\u0B95\u0B9F\u0BCD\u0B95\u0BBF\u0BB4\u0BAE\u0BC8','\u0B9A\u0BC6\u0BB5\u0BCD\u0BB5\u0BBE\u0BAF\u0BCD\u0B95\u0BCD\u0B95\u0BBF\u0BB4\u0BAE\u0BC8','\u0BAA\u0BC1\u0BA4\u0BA9\u0BCD\u0B95\u0BBF\u0BB4\u0BAE\u0BC8','\u0BB5\u0BBF\u0BAF\u0BBE\u0BB4\u0B95\u0BCD\u0B95\u0BBF\u0BB4\u0BAE\u0BC8','\u0BB5\u0BC6\u0BB3\u0BCD\u0BB3\u0BBF\u0B95\u0BCD\u0B95\u0BBF\u0BB4\u0BAE\u0BC8','\u0B9A\u0BA9\u0BBF\u0B95\u0BCD\u0B95\u0BBF\u0BB4\u0BAE\u0BC8'],
    dayNamesShort: ['\u0B9E\u0BBE\u0BAF\u0BBF\u0BB1\u0BC1','\u0BA4\u0BBF\u0B99\u0BCD\u0B95\u0BB3\u0BCD','\u0B9A\u0BC6\u0BB5\u0BCD\u0BB5\u0BBE\u0BAF\u0BCD','\u0BAA\u0BC1\u0BA4\u0BA9\u0BCD','\u0BB5\u0BBF\u0BAF\u0BBE\u0BB4\u0BA9\u0BCD','\u0BB5\u0BC6\u0BB3\u0BCD\u0BB3\u0BBF','\u0B9A\u0BA9\u0BBF'],
    dayNamesMin: ['\u0B9E\u0BBE','\u0BA4\u0BBF','\u0B9A\u0BC6','\u0BAA\u0BC1','\u0BB5\u0BBF','\u0BB5\u0BC6','\u0B9A'],
    weekHeader: '\u041D\u0435',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['ta']);

var i18nDatepickerTa = datepicker.regional['ta'];


/* Thai initialisation for the jQuery UI date picker plugin. */
/* Written by pipo (pipo@sixhead.com). */


datepicker.regional['th'] = {
    closeText: '\u0E1B\u0E34\u0E14',
    prevText: '&#xAB;&#xA0;\u0E22\u0E49\u0E2D\u0E19',
    nextText: '\u0E16\u0E31\u0E14\u0E44\u0E1B&#xA0;&#xBB;',
    currentText: '\u0E27\u0E31\u0E19\u0E19\u0E35\u0E49',
    monthNames: ['\u0E21\u0E01\u0E23\u0E32\u0E04\u0E21','\u0E01\u0E38\u0E21\u0E20\u0E32\u0E1E\u0E31\u0E19\u0E18\u0E4C','\u0E21\u0E35\u0E19\u0E32\u0E04\u0E21','\u0E40\u0E21\u0E29\u0E32\u0E22\u0E19','\u0E1E\u0E24\u0E29\u0E20\u0E32\u0E04\u0E21','\u0E21\u0E34\u0E16\u0E38\u0E19\u0E32\u0E22\u0E19',
    '\u0E01\u0E23\u0E01\u0E0E\u0E32\u0E04\u0E21','\u0E2A\u0E34\u0E07\u0E2B\u0E32\u0E04\u0E21','\u0E01\u0E31\u0E19\u0E22\u0E32\u0E22\u0E19','\u0E15\u0E38\u0E25\u0E32\u0E04\u0E21','\u0E1E\u0E24\u0E28\u0E08\u0E34\u0E01\u0E32\u0E22\u0E19','\u0E18\u0E31\u0E19\u0E27\u0E32\u0E04\u0E21'],
    monthNamesShort: ['\u0E21.\u0E04.','\u0E01.\u0E1E.','\u0E21\u0E35.\u0E04.','\u0E40\u0E21.\u0E22.','\u0E1E.\u0E04.','\u0E21\u0E34.\u0E22.',
    '\u0E01.\u0E04.','\u0E2A.\u0E04.','\u0E01.\u0E22.','\u0E15.\u0E04.','\u0E1E.\u0E22.','\u0E18.\u0E04.'],
    dayNames: ['\u0E2D\u0E32\u0E17\u0E34\u0E15\u0E22\u0E4C','\u0E08\u0E31\u0E19\u0E17\u0E23\u0E4C','\u0E2D\u0E31\u0E07\u0E04\u0E32\u0E23','\u0E1E\u0E38\u0E18','\u0E1E\u0E24\u0E2B\u0E31\u0E2A\u0E1A\u0E14\u0E35','\u0E28\u0E38\u0E01\u0E23\u0E4C','\u0E40\u0E2A\u0E32\u0E23\u0E4C'],
    dayNamesShort: ['\u0E2D\u0E32.','\u0E08.','\u0E2D.','\u0E1E.','\u0E1E\u0E24.','\u0E28.','\u0E2A.'],
    dayNamesMin: ['\u0E2D\u0E32.','\u0E08.','\u0E2D.','\u0E1E.','\u0E1E\u0E24.','\u0E28.','\u0E2A.'],
    weekHeader: 'Wk',
    dateFormat: 'dd/mm/yy',
    firstDay: 0,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['th']);

var i18nDatepickerTh = datepicker.regional['th'];


/* Tajiki (UTF-8) initialisation for the jQuery UI date picker plugin. */
/* Written by Abdurahmon Saidov (saidovab@gmail.com). */


datepicker.regional['tj'] = {
    closeText: '\u0418\u0434\u043E\u043C\u0430',
    prevText: '&#x3c;\u049A\u0430\u0444\u043E',
    nextText: '\u041F\u0435\u0448&#x3e;',
    currentText: '\u0418\u043C\u0440\u04EF\u0437',
    monthNames: ['\u042F\u043D\u0432\u0430\u0440','\u0424\u0435\u0432\u0440\u0430\u043B','\u041C\u0430\u0440\u0442','\u0410\u043F\u0440\u0435\u043B','\u041C\u0430\u0439','\u0418\u044E\u043D',
    '\u0418\u044E\u043B','\u0410\u0432\u0433\u0443\u0441\u0442','\u0421\u0435\u043D\u0442\u044F\u0431\u0440','\u041E\u043A\u0442\u044F\u0431\u0440','\u041D\u043E\u044F\u0431\u0440','\u0414\u0435\u043A\u0430\u0431\u0440'],
    monthNamesShort: ['\u042F\u043D\u0432','\u0424\u0435\u0432','\u041C\u0430\u0440','\u0410\u043F\u0440','\u041C\u0430\u0439','\u0418\u044E\u043D',
    '\u0418\u044E\u043B','\u0410\u0432\u0433','\u0421\u0435\u043D','\u041E\u043A\u0442','\u041D\u043E\u044F','\u0414\u0435\u043A'],
    dayNames: ['\u044F\u043A\u0448\u0430\u043D\u0431\u0435','\u0434\u0443\u0448\u0430\u043D\u0431\u0435','\u0441\u0435\u0448\u0430\u043D\u0431\u0435','\u0447\u043E\u0440\u0448\u0430\u043D\u0431\u0435','\u043F\u0430\u043D\u04B7\u0448\u0430\u043D\u0431\u0435','\u04B7\u0443\u043C\u044A\u0430','\u0448\u0430\u043D\u0431\u0435'],
    dayNamesShort: ['\u044F\u043A\u0448','\u0434\u0443\u0448','\u0441\u0435\u0448','\u0447\u043E\u0440','\u043F\u0430\u043D','\u04B7\u0443\u043C','\u0448\u0430\u043D'],
    dayNamesMin: ['\u042F\u043A','\u0414\u0448','\u0421\u0448','\u0427\u0448','\u041F\u0448','\u04B6\u043C','\u0428\u043D'],
    weekHeader: '\u0425\u0444',
    dateFormat: 'dd.mm.yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['tj']);

var i18nDatepickerTj = datepicker.regional['tj'];


/* Turkish initialisation for the jQuery UI date picker plugin. */
/* Written by Izzet Emre Erkan (kara@karalamalar.net). */


datepicker.regional['tr'] = {
    closeText: 'kapat',
    prevText: '&#x3C;geri',
    nextText: 'ileri&#x3e',
    currentText: 'bug\u00FCn',
    monthNames: ['Ocak','\u015Eubat','Mart','Nisan','May\u0131s','Haziran',
    'Temmuz','A\u011Fustos','Eyl\u00FCl','Ekim','Kas\u0131m','Aral\u0131k'],
    monthNamesShort: ['Oca','\u015Eub','Mar','Nis','May','Haz',
    'Tem','A\u011Fu','Eyl','Eki','Kas','Ara'],
    dayNames: ['Pazar','Pazartesi','Sal\u0131','\u00C7ar\u015Famba','Per\u015Fembe','Cuma','Cumartesi'],
    dayNamesShort: ['Pz','Pt','Sa','\u00C7a','Pe','Cu','Ct'],
    dayNamesMin: ['Pz','Pt','Sa','\u00C7a','Pe','Cu','Ct'],
    weekHeader: 'Hf',
    dateFormat: 'dd.mm.yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['tr']);

var i18nDatepickerTr = datepicker.regional['tr'];


/* Ukrainian (UTF-8) initialisation for the jQuery UI date picker plugin. */
/* Written by Maxim Drogobitskiy (maxdao@gmail.com). */
/* Corrected by Igor Milla (igor.fsp.milla@gmail.com). */


datepicker.regional['uk'] = {
    closeText: '\u0417\u0430\u043A\u0440\u0438\u0442\u0438',
    prevText: '&#x3C;',
    nextText: '&#x3E;',
    currentText: '\u0421\u044C\u043E\u0433\u043E\u0434\u043D\u0456',
    monthNames: ['\u0421\u0456\u0447\u0435\u043D\u044C','\u041B\u044E\u0442\u0438\u0439','\u0411\u0435\u0440\u0435\u0437\u0435\u043D\u044C','\u041A\u0432\u0456\u0442\u0435\u043D\u044C','\u0422\u0440\u0430\u0432\u0435\u043D\u044C','\u0427\u0435\u0440\u0432\u0435\u043D\u044C',
    '\u041B\u0438\u043F\u0435\u043D\u044C','\u0421\u0435\u0440\u043F\u0435\u043D\u044C','\u0412\u0435\u0440\u0435\u0441\u0435\u043D\u044C','\u0416\u043E\u0432\u0442\u0435\u043D\u044C','\u041B\u0438\u0441\u0442\u043E\u043F\u0430\u0434','\u0413\u0440\u0443\u0434\u0435\u043D\u044C'],
    monthNamesShort: ['\u0421\u0456\u0447','\u041B\u044E\u0442','\u0411\u0435\u0440','\u041A\u0432\u0456','\u0422\u0440\u0430','\u0427\u0435\u0440',
    '\u041B\u0438\u043F','\u0421\u0435\u0440','\u0412\u0435\u0440','\u0416\u043E\u0432','\u041B\u0438\u0441','\u0413\u0440\u0443'],
    dayNames: ['\u043D\u0435\u0434\u0456\u043B\u044F','\u043F\u043E\u043D\u0435\u0434\u0456\u043B\u043E\u043A','\u0432\u0456\u0432\u0442\u043E\u0440\u043E\u043A','\u0441\u0435\u0440\u0435\u0434\u0430','\u0447\u0435\u0442\u0432\u0435\u0440','\u043F\u2019\u044F\u0442\u043D\u0438\u0446\u044F','\u0441\u0443\u0431\u043E\u0442\u0430'],
    dayNamesShort: ['\u043D\u0435\u0434','\u043F\u043D\u0434','\u0432\u0456\u0432','\u0441\u0440\u0434','\u0447\u0442\u0432','\u043F\u0442\u043D','\u0441\u0431\u0442'],
    dayNamesMin: ['\u041D\u0434','\u041F\u043D','\u0412\u0442','\u0421\u0440','\u0427\u0442','\u041F\u0442','\u0421\u0431'],
    weekHeader: '\u0422\u0438\u0436',
    dateFormat: 'dd.mm.yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['uk']);

var i18nDatepickerUk = datepicker.regional['uk'];


/* Vietnamese initialisation for the jQuery UI date picker plugin. */
/* Translated by Le Thanh Huy (lthanhhuy@cit.ctu.edu.vn). */


datepicker.regional['vi'] = {
    closeText: '\u0110\u00F3ng',
    prevText: '&#x3C;Tr\u01B0\u1EDBc',
    nextText: 'Ti\u1EBFp&#x3E;',
    currentText: 'H\u00F4m nay',
    monthNames: ['Th\u00E1ng M\u1ED9t', 'Th\u00E1ng Hai', 'Th\u00E1ng Ba', 'Th\u00E1ng T\u01B0', 'Th\u00E1ng N\u0103m', 'Th\u00E1ng S\u00E1u',
    'Th\u00E1ng B\u1EA3y', 'Th\u00E1ng T\u00E1m', 'Th\u00E1ng Ch\u00EDn', 'Th\u00E1ng M\u01B0\u1EDDi', 'Th\u00E1ng M\u01B0\u1EDDi M\u1ED9t', 'Th\u00E1ng M\u01B0\u1EDDi Hai'],
    monthNamesShort: ['Th\u00E1ng 1', 'Th\u00E1ng 2', 'Th\u00E1ng 3', 'Th\u00E1ng 4', 'Th\u00E1ng 5', 'Th\u00E1ng 6',
    'Th\u00E1ng 7', 'Th\u00E1ng 8', 'Th\u00E1ng 9', 'Th\u00E1ng 10', 'Th\u00E1ng 11', 'Th\u00E1ng 12'],
    dayNames: ['Ch\u1EE7 Nh\u1EADt', 'Th\u1EE9 Hai', 'Th\u1EE9 Ba', 'Th\u1EE9 T\u01B0', 'Th\u1EE9 N\u0103m', 'Th\u1EE9 S\u00E1u', 'Th\u1EE9 B\u1EA3y'],
    dayNamesShort: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
    dayNamesMin: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
    weekHeader: 'Tu',
    dateFormat: 'dd/mm/yy',
    firstDay: 0,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''};
datepicker.setDefaults(datepicker.regional['vi']);

var i18nDatepickerVi = datepicker.regional['vi'];


/* Chinese initialisation for the jQuery UI date picker plugin. */
/* Written by Cloudream (cloudream@gmail.com). */


datepicker.regional['zh-CN'] = {
    closeText: '\u5173\u95ED',
    prevText: '&#x3C;\u4E0A\u6708',
    nextText: '\u4E0B\u6708&#x3E;',
    currentText: '\u4ECA\u5929',
    monthNames: ['\u4E00\u6708','\u4E8C\u6708','\u4E09\u6708','\u56DB\u6708','\u4E94\u6708','\u516D\u6708',
    '\u4E03\u6708','\u516B\u6708','\u4E5D\u6708','\u5341\u6708','\u5341\u4E00\u6708','\u5341\u4E8C\u6708'],
    monthNamesShort: ['\u4E00\u6708','\u4E8C\u6708','\u4E09\u6708','\u56DB\u6708','\u4E94\u6708','\u516D\u6708',
    '\u4E03\u6708','\u516B\u6708','\u4E5D\u6708','\u5341\u6708','\u5341\u4E00\u6708','\u5341\u4E8C\u6708'],
    dayNames: ['\u661F\u671F\u65E5','\u661F\u671F\u4E00','\u661F\u671F\u4E8C','\u661F\u671F\u4E09','\u661F\u671F\u56DB','\u661F\u671F\u4E94','\u661F\u671F\u516D'],
    dayNamesShort: ['\u5468\u65E5','\u5468\u4E00','\u5468\u4E8C','\u5468\u4E09','\u5468\u56DB','\u5468\u4E94','\u5468\u516D'],
    dayNamesMin: ['\u65E5','\u4E00','\u4E8C','\u4E09','\u56DB','\u4E94','\u516D'],
    weekHeader: '\u5468',
    dateFormat: 'yy-mm-dd',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: true,
    yearSuffix: '\u5E74'};
datepicker.setDefaults(datepicker.regional['zh-CN']);

var i18nDatepickerZhCn = datepicker.regional['zh-CN'];


/* Chinese initialisation for the jQuery UI date picker plugin. */
/* Written by SCCY (samuelcychan@gmail.com). */


datepicker.regional['zh-HK'] = {
    closeText: '\u95DC\u9589',
    prevText: '&#x3C;\u4E0A\u6708',
    nextText: '\u4E0B\u6708&#x3E;',
    currentText: '\u4ECA\u5929',
    monthNames: ['\u4E00\u6708','\u4E8C\u6708','\u4E09\u6708','\u56DB\u6708','\u4E94\u6708','\u516D\u6708',
    '\u4E03\u6708','\u516B\u6708','\u4E5D\u6708','\u5341\u6708','\u5341\u4E00\u6708','\u5341\u4E8C\u6708'],
    monthNamesShort: ['\u4E00\u6708','\u4E8C\u6708','\u4E09\u6708','\u56DB\u6708','\u4E94\u6708','\u516D\u6708',
    '\u4E03\u6708','\u516B\u6708','\u4E5D\u6708','\u5341\u6708','\u5341\u4E00\u6708','\u5341\u4E8C\u6708'],
    dayNames: ['\u661F\u671F\u65E5','\u661F\u671F\u4E00','\u661F\u671F\u4E8C','\u661F\u671F\u4E09','\u661F\u671F\u56DB','\u661F\u671F\u4E94','\u661F\u671F\u516D'],
    dayNamesShort: ['\u5468\u65E5','\u5468\u4E00','\u5468\u4E8C','\u5468\u4E09','\u5468\u56DB','\u5468\u4E94','\u5468\u516D'],
    dayNamesMin: ['\u65E5','\u4E00','\u4E8C','\u4E09','\u56DB','\u4E94','\u516D'],
    weekHeader: '\u5468',
    dateFormat: 'dd-mm-yy',
    firstDay: 0,
    isRTL: false,
    showMonthAfterYear: true,
    yearSuffix: '\u5E74'};
datepicker.setDefaults(datepicker.regional['zh-HK']);

var i18nDatepickerZhHk = datepicker.regional['zh-HK'];


/* Chinese initialisation for the jQuery UI date picker plugin. */
/* Written by Ressol (ressol@gmail.com). */


datepicker.regional['zh-TW'] = {
    closeText: '\u95DC\u9589',
    prevText: '&#x3C;\u4E0A\u6708',
    nextText: '\u4E0B\u6708&#x3E;',
    currentText: '\u4ECA\u5929',
    monthNames: ['\u4E00\u6708','\u4E8C\u6708','\u4E09\u6708','\u56DB\u6708','\u4E94\u6708','\u516D\u6708',
    '\u4E03\u6708','\u516B\u6708','\u4E5D\u6708','\u5341\u6708','\u5341\u4E00\u6708','\u5341\u4E8C\u6708'],
    monthNamesShort: ['\u4E00\u6708','\u4E8C\u6708','\u4E09\u6708','\u56DB\u6708','\u4E94\u6708','\u516D\u6708',
    '\u4E03\u6708','\u516B\u6708','\u4E5D\u6708','\u5341\u6708','\u5341\u4E00\u6708','\u5341\u4E8C\u6708'],
    dayNames: ['\u661F\u671F\u65E5','\u661F\u671F\u4E00','\u661F\u671F\u4E8C','\u661F\u671F\u4E09','\u661F\u671F\u56DB','\u661F\u671F\u4E94','\u661F\u671F\u516D'],
    dayNamesShort: ['\u5468\u65E5','\u5468\u4E00','\u5468\u4E8C','\u5468\u4E09','\u5468\u56DB','\u5468\u4E94','\u5468\u516D'],
    dayNamesMin: ['\u65E5','\u4E00','\u4E8C','\u4E09','\u56DB','\u4E94','\u516D'],
    weekHeader: '\u5468',
    dateFormat: 'yy/mm/dd',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: true,
    yearSuffix: '\u5E74'};
datepicker.setDefaults(datepicker.regional['zh-TW']);

var i18nDatepickerZhTw = datepicker.regional['zh-TW'];

//UPDATE VERSION BELOW THIS LINE
var getUrl = bab_jQuery_extractUrlParams();
datepicker.setDefaults(datepicker.regional[getUrl.lang]);

}));