2017-01-05: Version 1.12.4.0

- Update jQuery version to 1.12.4
- Update jQueryUI to version 1.12.1
- Update jQueryMobile to version 1.4.5
- Update jQueryMigrate to version 1.4.1

2014-07-08: Version 1.11.1.0

- Update jQuery version to 1.11.1
- Update jQueryUI to version 1.11.0
- Update jQueryMobile to version 1.4.3

2014-03-16: Version 1.9.1.1

- Fixed embedded stylesheets to match version of jquery-ui

2012-10-31: Version 1.9.1.0

- Update jQuery version, jQuery UI and jQuery mobile.
- The default system now use CDN.

2012-04-13: Version 1.7.1.4

- add jquery mobile

2012-02-27: Version 1.7.1.2

- add jquery lightbox


2011-02-04: Version 1.4.4.0

2010-08-01: Version 1.3.2.7
    - Automatic call to noConflict if needed.

2009-09-07: Version 1.3.2.3


2009-08-13: Version 1.3.2.1
    - Compatibility PHP 5.3 : addition of ; in ini file

2009-05-18: Version 1.3.1.1
    - Update to ui.datepicker.js 1.7.1 (datepicker bug fix)

2009-01-23: Version 1.3.1.0
    - Updated to Jquery 1.3.1 and jquery ui 1.6rc5

2008-02-04: Version 1.2.2
    - Initial release
